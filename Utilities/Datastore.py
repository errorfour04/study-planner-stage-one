from datetime import datetime
from Utilities.readwrite import write_file, read_file, update_file, overwrite_file

class Datastore:
    # Constructor
    def __init__(self):
        pass

    @staticmethod
    def add_profile(title):
        info = [title]
        write_file('profiles', info)

    # All study profiles belong to one imaginary user in stage one
    @staticmethod
    def get_study_profiles():
        from Model.StudyProfile import StudyProfile

        profiles = []

        for profile in read_file('profiles'):
            p = StudyProfile(profile[0])
            profiles.append(p)

        return profiles

    @staticmethod
    def add_module(module, profile_title):
        from Model.Module import Module

        info = []
        info = ([
            module.get_name(),
            profile_title
        ])
        write_file('modules', info)

    @staticmethod
    def get_modules(profile_title):
        from Model.Module import Module
        modules = []

        info = [module for module in read_file('modules') if (
            # Get modules for a specific study profile
            len(module) > 0 and module[len(module) - 1] == profile_title
        )]

        for module in info:
            if len(module) >= 2:
                m = Module(module[0])
                modules.append(m)

        return modules

    @staticmethod
    def add_assignment(assignment, module_title):
        from Model.Assignment import Assignment

        info = []
        info = ([assignment.get_title(),
                 assignment.get_assignment_type().value,
                 assignment.get_deadline().strftime('%d/%m/%y'),
                 assignment.get_weighting()
         ])
        info.append(module_title)
        write_file('assignments', info)
        return True

    @staticmethod
    def get_assignments(module_title):
        from Model.Assignment import Assignment
        assignments = []

        info = [assignment for assignment in read_file('assignments') if (
            # Get assignments for a specific module
            len(assignment) > 0 and assignment[len(assignment) - 1] == module_title
        )]

        for assignment in info:
            if len(assignment) >= 5:
                a = Assignment(assignment[0], assignment[1], assignment[2], assignment[3])
                assignments.append(a)

        return assignments

    # Add Task
    @staticmethod
    def add_task(task, assignment_title):
        from Model.Task import Task

        info = ([   task.get_title(),
                    task.get_criterion(),
                    task.get_criterion_target(),
                    task.get_start_date().strftime('%d/%m/%y %H:%M'),
                    task.get_end_date().strftime('%d/%m/%y %H:%M'),
                    task.get_amount_completed(),
                    task.get_allocated_time(),
                    int(task.is_completed())
                ])
        info.append(assignment_title)
        write_file('tasks', info)
        return True

    # Get Tasks
    @staticmethod
    def get_tasks(assignment_title):
        from Model.Task import Task
        tasks = []

        info = [task for task in read_file('tasks') if (
            # Get tasks for a specific assignment
            len(task) > 0 and task[len(task) - 1] == assignment_title
        )]

        for task in info:
            if len(task) >= 9:
                t = Task(task[0], task[1], task[2], task[3], task[4], task[5], task[6], task[7])
                tasks.append(t)

        return tasks

    @staticmethod
    def get_task(title):
        from Model.Task import Task

        for task in read_file('tasks'):
            # Get task by title
            if len(task) > 0 and len(task) >= 9 and task[0] == title:
                return Task(task[0], task[1], task[2], task[3], task[4], task[5], task[6], task[7])

        return None
        # Task('Dummy', 'Reading', 10, '10/03/19 00:00', '10/03/19 00:00', 10, 0, 0)

    # Update Task
    @staticmethod
    def update_task(updated_task):
        from Model.Task import Task
        tasks = read_file('tasks')
        saved_task = next(
            task for task in tasks if task is not None and task[0] == updated_task.get_title() and len(task) >= 1
        )
        index = tasks.index(saved_task)

        # Save the updated task
        saved_task[0] = updated_task.get_title()
        saved_task[1] = updated_task.get_criterion()
        saved_task[2] = updated_task.get_criterion_target()
        saved_task[3] = updated_task.get_start_date().strftime('%d/%m/%y %H:%M')
        saved_task[4] = updated_task.get_end_date().strftime('%d/%m/%y %H:%M')
        saved_task[5] = updated_task.get_amount_completed()
        saved_task[6] = updated_task.get_allocated_time()
        saved_task[7] = int(updated_task.is_completed())

        update_file('tasks', saved_task, index)

    # Add Dependency
    @staticmethod
    def add_dependency(depends, task_title) -> bool:
        info = []
        info.append(depends.get_title())
        info.append(task_title)
        write_file('dependencies', info)
        return True


    # Get Dependencies
    @staticmethod
    def get_dependencies(task_title):
        info = [dependency[0] for dependency in read_file('dependencies') if (
            # Get dependencies for a specific task
            len(dependency) > 0 and dependency[len(dependency) - 1] == task_title
        )]

        # Get dependee tasks based on their titles
        dependencies = []
        for item in info:
            dependencies.append(Datastore.get_task(item))

        return dependencies

    # Add Note
    @staticmethod
    def add_note(text, task_title):
        info = [text]
        info.append(task_title)
        write_file('tasknotes', info)
        return True

    # Get Notes
    @staticmethod
    def get_notes(task_title):
        info = [note[0] for note in read_file('tasknotes') if (
            # Get notes for a specific task
            len(note) > 0 and note[len(note) - 1] == task_title
        )]
        return info


    @staticmethod
    def add_mil(name, deadline, tasks_to_complete, assignment_name):
        info = []

        info = [name, deadline, tasks_to_complete, False, 0.0, assignment_name]

        write_file("milestones", info)

        return True

    @staticmethod
    def add_activity(name, associated_tasks, study_type, assignment_name, contribution):
        # name, associated_tasks, study_type, hours spent
        info = [name, associated_tasks, study_type, contribution, 0, assignment_name]
        write_file("activities", info)
        pass

    @staticmethod
    def get_activities(assignment_title):
        return [activity for activity in read_file('activities') if activity[len(activity) - 1] == assignment_title]

    @staticmethod
    def set_activity_progress(name, time_spent):
        # time spent will be incremented on activity
        # progress will be passed to the tasks

        activity_data = read_file("activities")
        activity_obj = None
        pos_in_file = -1

        print("name to search", name)
        for i in activity_data:
            for j in i:
                if (i.index(j) == 0 and j == name):
                    activity_obj = i
            pos_in_file += 1
        print("activity",activity_obj)

        activity_tasks = activity_obj[1][1:-1].replace('\'', '').split(',')
        for i in activity_tasks:
            if i[0] == ' ':
                index = activity_tasks.index(i)
                i = i[1:len(i)]
                activity_tasks[index] = i


        # update time spent
        print(activity_obj[4], " + ", int(time_spent))
        new_hours = int(activity_obj[4]) + int(time_spent)
        activity_obj[4] = str(new_hours)
        print(activity_obj)
        print(pos_in_file)
        overwrite_file("activities", activity_obj, pos_in_file)
    
    @staticmethod
    def get_milestones_a(assignment_title):
        milestones = []
        milestone_names = [
            milestone[0] for milestone in Datastore.get_milestones() if (
                milestone[len(milestone) - 1] == assignment_title
            )
        ]
        
        for milestone in milestone_names:
            milestones.append(Datastore.get_milestone(milestone))
        
        return milestones

    @staticmethod
    def get_milestone(milestone_name):
        from Model.Milestone import Milestone
        
        milestones = read_file("milestones")
        milestone = None
        milestone_obj = []
        for i in milestones:
            for j in i:
                if(i.index(j) == 0 and j == milestone_name):
                    milestone_obj = i


        if(milestone_obj != []):
            #get deadlines
            milestone_deadline = datetime.strptime(milestone_obj[1], "%Y-%m-%d %H:%M:%S")

            # get tasks
            milestone_tasks = milestone_obj[2][1:-1].replace('\'', '').split(',')
            for i in milestone_tasks:
                if i[0] == ' ':
                    index = milestone_tasks.index(i)
                    i = i[1:len(i)]
                    milestone_tasks[index] = i



            # get complete status
            milestone_complete = False
            if(milestone_obj[3] == 'True'):
                milestone_complete = True

            # get progress
            milestone_progress = float(milestone_obj[4])

            milestone = Milestone(milestone_obj[0], milestone_deadline,
                                  milestone_tasks, milestone_complete, milestone_progress)

        return milestone


    @staticmethod
    def get_assignment_from_milestone(milestone_name):
        milestones = read_file("milestones")
        milestone = None
        milestone_obj = []
        for i in milestones:
            for j in i:
                if(i.index(j) == 0 and j == milestone_name):
                    milestone_obj = i
        return milestone_obj[5]
        
    # Get Milestone
    @staticmethod
    def get_milestones():
        milestones = read_file("milestones")
        return milestones
