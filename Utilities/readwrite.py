import csv

def read_file(filename):
    with open(('./data/' + filename + '.csv'), 'r') as infile:
        reader = csv.reader(infile)
        array = [row for row in reader]
    return array


def write_file(filename, new_data):
    array = read_file(filename)
    with open('./data/' + filename + '.csv', 'w', newline='') as outfile:
        writer = csv.writer(outfile)
        array.append(new_data)
        writer.writerows(array)

def update_file(filename, new_data, index):
    array = read_file(filename)
    with open('./data/'+ filename + '.csv', 'w', newline='') as outFile:
        writer = csv.writer(outFile)
        array[index] = new_data
        writer.writerows(array)
        
def overwrite_file(filename, new_data, line):
    array = read_file(filename)
    with open('./data/' + filename + '.csv', 'w', newline='') as outfile:
        writer = csv.writer(outfile)
        array[line] = new_data
        writer.writerows(array)