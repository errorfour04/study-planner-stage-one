from datetime import datetime
from enum import Enum, unique

from Utilities.Datastore import Datastore

@unique
class TaskType(Enum):
    PROGRAMMING = 'Programming'
    READING = 'Reading'
    WRITING = 'Writing'

class Task:
    # Substitute for Criteria Enum
    __title = ''
    __criterion = ''
    __criterion_target = 0
    __start_date = datetime.now()
    __end_date = datetime.now()
    __amount_completed = 0
    __allocated_time = 0
    __dependent_on = []
    __notes = []
    __completed = False

    # Constructor
    def __init__(
            self,
            title,
            criterion,
            criterion_target,
            start_date,
            end_date,
            amount_completed,
            allocated_time,
            completed
    ):
        self.__title = str(title)

        valid_criteria = [str(value).lower() for value in TaskType.__members__]
        if str(criterion).lower() not in valid_criteria:
            raise ValueError("Task type needs to be one of {}".format(valid_criteria))

        self.__criterion = criterion
        self.__criterion_target = abs(int(criterion_target))

        self.__start_date = datetime.strptime(start_date, '%d/%m/%y %H:%M')
        self.__end_date = datetime.strptime(end_date, '%d/%m/%y %H:%M')

        # Datetime validation
        if self.__end_date < self.__start_date:
            tmp = self.__end_date
            self.__end_date = self.__start_date
            self.__start_date = tmp

        self.__amount_completed = abs(int(amount_completed))
        self.__allocated_time = abs(int(allocated_time))
        self.__dependent_on = Datastore.get_dependencies(self.__title)
        self.__notes = Datastore.get_notes(self.__title)
        self.__completed = bool(int(completed))

    # Accessors
    def get_title(self):
        return self.__title

    def get_criterion(self):
        return self.__criterion

    def get_criterion_target(self):
        return self.__criterion_target

    def get_start_date(self):
        return self.__start_date

    def get_end_date(self):
        return self.__end_date

    def get_amount_completed(self):
        return self.__amount_completed

    def get_allocated_time(self):
        return self.__allocated_time

    def get_dependent_on(self):
        return self.__dependent_on

    def get_notes(self):
        return self.__notes

    def is_completed(self):
        return self.__completed

    # Instance Methods
    def add_dependency(self, task):
        if Datastore.add_dependency(task, self.__title):
            dependency = Datastore.get_task(task.get_title())
            self.__dependent_on.append(dependency)

    def add_note(self, text):
        if Datastore.add_note(text, self.__title):
            self.__notes.append(text)
            return True
        return False

    def update_status(self, amount):
        if self.__start_date > datetime.now():
            self.__start_date = datetime.now()

        # Amount Completed shouldn't exceed the target value
        if self.__amount_completed + amount > self.__criterion_target:
            self.__amount_completed += amount
            self.__criterion_target = self.__amount_completed
        else:
            self.__amount_completed += amount

        Datastore.update_task(self)

    def increment_time(self, amount):
        self.__allocated_time += abs(amount)
        Datastore.update_task(self)

    def set_complete(self):
        self.__completed = True
        Datastore.update_task(self)