from Utilities.Datastore import Datastore

from Model.Module import Module
from Model.Assignment import Assignment

class StudyProfile:
    __title = ''
    __modules = None

    def __init__(self, title):
        self.__title = title

        self.__modules = []
        self.__modules = Datastore.get_modules(self.__title)

    # Accessors
    def get_title(self):
        return self.__title

    def get_modules(self):
        return self.__modules

    # Instance Methods
    def add_module(self, module):
        Datastore.add_module(module, self.__title)
        self.__modules.append(module)

    def load_semester_file(self, filename):
        # from Model.Module import Module

        module_titles = []
        assignments = []

        try:
            # Create Semester File
            with open(filename, 'r', newline='') as semester_file:
                for line in semester_file:
                    if line[-1] == '\n':
                        line = line[:-1]
                    info = line.split(',')

                    # Get Module Titles
                    if info[0] not in module_titles:
                        module_titles.append(info[0])

                    # Get Assignment Info
                    assignments.append(info)

                semester_file.close()

                # Create Module Objects
                for title in module_titles:
                    m = Module(title)
                    self.add_module(m)

                # Create Assignments
                for mod in self.get_modules():
                    for assignment in assignments:
                        if assignment[0] == mod.get_name():
                            a = Assignment(
                                assignment[1],
                                assignment[2],
                                assignment[3],
                                assignment[4]
                            )
                            mod.add_assignment(a)

                return True
        except:
            return False