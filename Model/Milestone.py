import datetime
from Utilities.Datastore import Datastore

class Milestone:



    # attributes
    __name = ""
    __deadline = None # NEEDS TO BE VALIDED ONCE TASK IS DONE
    __tasks_to_complete = None
    __complete = None
    __progress = None


    # validate date time
    def __init__(self, name, deadline, tasks_to_complete, complete = False, progress = 0.0):
        self.__name = name
        self.__deadline = deadline
        self.__tasks_to_complete = []
        #print("a",tasks_to_complete)
        for task in tasks_to_complete:
            self.__tasks_to_complete.append(Datastore.get_task(task))
         #    print(i)

        # print("c: ",count)
        self.__complete = complete
        self.__progress = progress



    def get_name(self):
        return self.__name

    def get_deadline(self):
        return self.__deadline

    def get_progress(self):
        count = 0
        for i in self.__tasks_to_complete:
            if(i.is_completed()):
                count += 1

        self.__progress = (count / len(self.__tasks_to_complete))*100

        return self.__progress

    def get_tasks_to_complete(self):
        return self.__tasks_to_complete

    def get_complete(self):
        return self.__complete


    def set_task_complete(self, task_name):
        index_of_task = self.__tasks_to_complete.index(task_name)
        self.__tasks_to_complete[index_of_task].setComplete()

        # update progress
        count = 0
        for i in self.__tasks_to_complete:
            if self.__tasks_to_complete[i].is_completed() == True:
                count += 1


        self.progress = count / len(self.__tasks_to_complete)

        if self.progress == 1.0:
            self.__complete = True


    def toString(self):
        return "Name: ", self.__name




