from datetime import datetime
from enum import Enum, unique

from Utilities.Datastore import Datastore
from Model.Task import Task

@unique
class AssignmentType(Enum):
    EXAM = 'Exam'
    COURSEWORK = 'Coursework'

class Assignment:
    __title = ''
    __assignment_type = ''
    __tasks = None
    __deadline = datetime.now()
    __weighting = 0
    __milestones = None
    __activities = None


    # Constructor
    def __init__(
            self,
            title,
            assignment_type,
            deadline,
            weighting
    ):
        self.__title = str(title)

        # Validate Type input
        if str(assignment_type).lower() not in ['coursework'.lower(), 'exam'.lower()]:
            raise ValueError("Assignment type needs to be either 'coursework' or 'exam'")

        self.__assignment_type = AssignmentType.COURSEWORK if \
            str(assignment_type).lower() == 'coursework' else \
            AssignmentType.EXAM

        self.__deadline = datetime.strptime(deadline, '%d/%m/%y')
        self.__weighting = int(weighting)

        self.__milestones = []
        self.__milestones = Datastore.get_milestones_a(self.__title)

        self.__activities = []
        self.__activities = Datastore.get_activities(self.__title)

        self.__tasks = []
        for task in Datastore.get_tasks(self.__title):
            self.__tasks.append(task)

    # Accessor Methods
    def get_title(self):
        return self.__title

    def get_assignment_type(self):
        return self.__assignment_type

    def get_tasks(self):
        return self.__tasks

    def get_deadline(self):
        return self.__deadline

    def is_complete(self):
        return self.__deadline <= datetime.now()

    def get_weighting(self):
        return self.__weighting

    def get_milestones(self):
        return self.__milestones

    def get_activities(self):
        return self.__activities

    def update_activities(self):
        self.__activities = Datastore.get_activities(self.__title)

    # Instance Methods
    def add_task(self, task):
        if Datastore.add_task(task, self.__title):
            self.__tasks.append(task)
            return True
        return False

    def add_milestone(self, milestone):
        self.__milestones.append(milestone)

    def set_new_deadline(self, day, month, year):
        self.__deadline = datetime(year, month, day)

        
# a = Assignment('stuff', 'exam', 0, '06/03/19', 0)
# print(a.get_assignment_type())
# a.set_new_deadline(4, 4, 2019)
# # t = Task('Rough',0,0,0,0,0,0,0,0)
# # for task in a.get_tasks(): print(task.get_title())
#
# # a.add_task(t)
# # print('\n')
# # for task in a.get_tasks(): print(task.get_title())