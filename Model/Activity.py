class Activity:

    __name = None
    __study_type = None
    __associated_tasks = None
    __time_taken = None
    __progress = None

    def __init__(self, name, study_type, associated_tasks, time_taken, progress):
        self.__name = name
        self.__study_type = study_type
        self.__associated_tasks = associated_tasks
        self.__time_taken = time_taken
        self.__progress = progress

    def get_name(self):
        return self.__name

    def get_study_type(self):
        return self.__study_type

    def get_associated_tasks(self):
        return self.__associated_tasks

    def get_time_taken(self):
        return self.__time_taken

    def get_progress(self):
        return self.__progress


