from Utilities.Datastore import Datastore

class Module:
    __name = None
    __assignments = None

    def __init__(self, name):
        self.__name = name

        self.__assignments = []
        self.__assignments = Datastore.get_assignments(name)

    def get_name(self):
        return self.__name

    def get_num_exams(self):
        count = 0
        for assignment in self.__assignments:
            if str(assignment.get_assignment_type().value).lower() == 'exam':
                count += 1

        return count

    def get_num_coursework(self):
        count = 0
        for assignment in self.__assignments:
            if str(assignment.get_assignment_type().value).lower() == 'coursework':
                count += 1

        return count

    def get_assignments(self):
        return self.__assignments

    def add_assignment(self, assignment):
        Datastore.add_assignment(assignment, self.__name)
        self.__assignments.append(assignment)

# test = Module("Systems Engineering")
# print(test.get_name())
# print(test.get_assignment())
