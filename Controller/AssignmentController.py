from datetime import datetime

from Model.Assignment import Assignment
from Model.Task import Task
from Model.Milestone import Milestone

from View.AssignmentWindow import AssignmentWindow
from View.AddTaskWindow import AddTaskWindow
from View.CreateMilestoneWindow import CreateMilestoneWindow
from View.ViewMilestoneWindow import ViewMilestoneWindow


class AssignmentController:
    current_assignment = None
    assignment_window = None
    add_task_window = None
    activities = []

    def __init__(self):
        pass

    @staticmethod
    def display_assignment_window(module, assignment):
        data = dict(
            module=module,
            title=assignment.get_title(),
            type=assignment.get_assignment_type().value,
            weight=assignment.get_weighting(),
            deadline=assignment.get_deadline().strftime("%d %b %y"),
            milestones=[milestone.get_name() for milestone in assignment.get_milestones()],
            milestone_progress=[int(milestone.get_progress()) for milestone in assignment.get_milestones()],
            tasks=[]
        )

        for task in assignment.get_tasks():
            try:
                completion = int(task.get_amount_completed() * 100 / task.get_criterion_target())
            except:
                completion = 0

            fmt_date = task.get_end_date().strftime("%d %b %y")
            data['tasks'].append([fmt_date, task.get_title(), completion])

        AssignmentController.activities = assignment.get_activities()
        AssignmentController.current_assignment = assignment
        AssignmentController.assignment_window = AssignmentWindow(data)
        AssignmentController.assignment_window.display()

    @staticmethod
    def load_task(index):
        from Controller.TaskController import TaskController

        if len(AssignmentController.current_assignment.get_tasks()) >= (index + 1):
            AssignmentController.assignment_window.root.withdraw()
            TaskController.display_task_window(
                AssignmentController.current_assignment.get_title(),
                AssignmentController.current_assignment.get_tasks()[index]
            )

    @staticmethod
    def display_add_task_window():
        data = dict(tasks=[task.get_title() for task in AssignmentController.current_assignment.get_tasks()])

        AssignmentController.add_task_window = AddTaskWindow(data)
        AssignmentController.add_task_window.popup()

    # Set the start date to a sensible time
    @staticmethod
    def manage_start_date(indices):
        dependencies = []
        for i in indices:
            dependencies.append(AssignmentController.get_tasks()[i].get_end_date())

        # Make the latest date the first in the array
        dependencies.sort(reverse=True)

        # Update the date in the add task window
        if len(dependencies) > 0:
            date = dependencies[0].strftime("%d/%m/%y %H:%M")
            AssignmentController.add_task_window.set_start_date(date)
        else:
            AssignmentController.add_task_window.set_start_date(None)




    @staticmethod
    def add_task(title, criterion, start_date, end_date, quantity, dependencies=None):
        t = Task(title, criterion, quantity, start_date, end_date, 0, 0, False)
        if AssignmentController.current_assignment.add_task(t):
            if dependencies:
                # Add dependencies based on the selection indexes received from window
                for i in dependencies:
                    # get the recently created task as it is stored by the assignment
                    t.add_dependency(AssignmentController.get_tasks()[i])

            # Update assignment window
            AssignmentController.refresh_tasks()
            AssignmentController.add_task_window.root.destroy()

    # Display Create Milestone Window
    @staticmethod
    def display_create_milestone_window():
        create_milestone_window = CreateMilestoneWindow(
            AssignmentController.current_assignment.get_title()
        )
        create_milestone_window.popup()

    # Display View Mileston Window
    @staticmethod
    def display_view_milestone_window(index):
        view_milestone_window = ViewMilestoneWindow(
            AssignmentController.get_milestones()[index].get_name()
        )
        AssignmentController.assignment_window.hide()
        view_milestone_window.display()

    # Add Milestone
    @staticmethod
    def add_milestone(milestone):
        AssignmentController.current_assignment.add_milestone(milestone)
        AssignmentController.refresh_milestones()

    # Update the Assignment window to show current info about milestones
    @staticmethod
    def refresh_milestones():
        AssignmentController.assignment_window.populate_milestones(
            [milestone.get_name() for milestone in AssignmentController.get_milestones()],
            [int(milestone.get_progress()) for milestone in AssignmentController.current_assignment.get_milestones()]
        )

    # Update the Assignment window to show current info about tasks
    @staticmethod
    def refresh_tasks():
        tasks = []
        for task in AssignmentController.get_tasks():
            try:
                completion = int(task.get_amount_completed() * 100 / task.get_criterion_target())
            except:
                completion = 0

            fmt_date = task.get_end_date().strftime("%d %b %y")
            tasks.append([fmt_date, task.get_title(), completion])

        AssignmentController.assignment_window.populate_tasks(tasks)

    @staticmethod
    def get_tasks():
        return AssignmentController.current_assignment.get_tasks()

    @staticmethod
    def get_milestones():
        return AssignmentController.current_assignment.get_milestones()

    @staticmethod
    def get_activities():
        return AssignmentController.current_assignment.get_activities()

    @staticmethod
    def update_activities():
        AssignmentController.current_assignment.update_activities()
