# from Model.Module import Module
from View.ModuleWindow import ModuleWindow


class ModuleController:
    current_module = None
    module_window = None

    def __init__(self):
        pass

    @staticmethod
    def display_module_window(profile, module):
        data = dict(
            profile=profile,
            moduletitle=module.get_name(),
            assignments=[assignment.get_title() for assignment in module.get_assignments()],
            types=[assignment.get_assignment_type().value for assignment in module.get_assignments()]
        )

        ModuleController.current_module = module
        ModuleController.module_window = ModuleWindow(data)
        ModuleController.module_window.display()

    @staticmethod
    def load_assignment(index):
        from Controller.AssignmentController import AssignmentController
        AssignmentController.display_assignment_window(
            ModuleController.current_module.get_name(),
            ModuleController.current_module.get_assignments()[index]
        )
        ModuleController.module_window.hide()

