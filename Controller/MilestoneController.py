from Model.Milestone import Milestone
from Utilities.Datastore import Datastore

import datetime

class MilestoneController:

    __MilestoneSet = []

    def __init__(self):
        pass

    def createMilestone(self, name, deadline, tasks_to_complete, assignment_name):
        from Controller.AssignmentController import AssignmentController
        #assignment_deadline = Datastore.get_assignment(assignment_name).get_deadline()

        new_milestone = Milestone(name, deadline, tasks_to_complete, False, 0.0)

        self.__MilestoneSet.append(new_milestone)
        Datastore.add_mil(name, deadline, tasks_to_complete, assignment_name)
        AssignmentController.add_milestone(new_milestone)

        return True

    def add_milestone(self, milestone_name):
        self.__MilestoneSet.append(milestone_name)

    def get_assignment(self, milestone_name):
        return Datastore().get_assignment_from_milestone(milestone_name)

    def get_milestone(self, name):
        # access Milestone.csv using datastore to create an object
        return Datastore().get_milestone(name)

    def set_task_complete(self, name):
        Datastore().complete_task(name)

