from Model.Activity import Activity
from Utilities.Datastore import Datastore


class ActivityController:

    def __init__(self):
        pass


    def create_activity(self, name, associated_tasks, study_type, assignment_name, contribution):
        from Controller.TaskController import TaskController

        Datastore.add_activity(name, associated_tasks, study_type, assignment_name, contribution)
        TaskController.update_activities()


    def set_activity_progress(self, activity_name, time_spent):
        from Controller.TaskController import TaskController

        Datastore.set_activity_progress(activity_name, time_spent)
        TaskController.update_activities()
        TaskController.complete_activity(time_spent)

