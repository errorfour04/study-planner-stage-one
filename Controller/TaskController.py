import ast
from datetime import datetime
from View.TaskWindow import TaskWindow
from View.AddNoteWindow import AddNoteWindow

from View.CreateActivityWindow import CreateActivityWindow
from View.ActivityTimeWindow import ActivityTimeWindow

from Model.Task import Task

class TaskController:
    current_task = None
    task_window = None
    add_note_window = None
    activities = None
    selected_activity_index = 0

    def __init__(self):
        pass

    @staticmethod
    def display_task_window(assignment, task):
        data = dict(
            assignment=assignment,
            title=task.get_title(),
            criterion=task.get_criterion(),
            amt_completed=task.get_amount_completed(),
            target=task.get_criterion_target(),
            start=task.get_start_date().strftime("%d %b %y at %H:%M"),
            end=task.get_end_date().strftime("%d %b %y at %H:%M"),
            time=task.get_allocated_time(),
            dep_title=[dep.get_title() for dep in task.get_dependent_on()],
            notes=task.get_notes(),
            completed=task.is_completed(),
            started=task.get_start_date() <= datetime.now(),
            ended=task.get_end_date() <= datetime.now(),
            activities=[]
        )

        TaskController.current_task = task      # Every subwindow uses same task
        data['activities'] = TaskController.get_activities()
        TaskController.task_window = TaskWindow(data)
        TaskController.task_window.display()

    @staticmethod
    def display_add_note_window():
        TaskController.add_note_window = AddNoteWindow()
        TaskController.add_note_window.popup()

    @staticmethod
    def add_note(text):
        if TaskController.current_task.add_note(text):
            # Update the Task Window
            TaskController.task_window.populate_notes(TaskController.current_task.get_notes())
            TaskController.add_note_window.root.destroy()

    @staticmethod
    def complete_task():
        TaskController.current_task.set_complete()
        TaskController.task_window.update_info(True)

    @staticmethod
    def add_activity():
        from Controller.AssignmentController import AssignmentController

        # Don't let the student add an activity if there are incomplete dependencies
        if TaskController.current_task.get_dependent_on and \
                next((dep for dep in TaskController.current_task.get_dependent_on() if not dep.is_completed()), None) \
                        is None:
            # Display Create Activity Window
            create_activity_window = CreateActivityWindow(
                AssignmentController.current_assignment.get_title()
            )
            create_activity_window.popup()
        else:
            TaskController.task_window.display_add_activity_error()

    @staticmethod
    def display_activity_time_window(index):
        from Controller.AssignmentController import AssignmentController
        # Get the corresponding activity title
        activity_time_window = ActivityTimeWindow(
            TaskController.get_activities()[index][0]
        )
        activity_time_window.popup()

        # Determines which activity has been selected
        TaskController.selected_activity_index = index

    @staticmethod
    def complete_activity(time_spent):
        from Controller.AssignmentController import AssignmentController

        # Get the required activity from the activity name
        activity = next(
            (activity for activity in AssignmentController.get_activities() if (
                # If the activity names match
                activity[0] == TaskController.get_activities()[TaskController.selected_activity_index][0]
            )), None
        )

        # Get the required tasks from the activity acquired above
        tasks = ast.literal_eval(activity[1])

        # Modify the task information accordingly
        for task in AssignmentController.get_tasks():
            if task.get_title() in tasks:
                task.update_status(
                    (TaskController.get_activities()[TaskController.selected_activity_index])[1]
                )
                task.increment_time(time_spent)

        TaskController.task_window.update_info(
            completed=TaskController.current_task.is_completed(),
            amt_completed=TaskController.current_task.get_amount_completed(),
            target=TaskController.current_task.get_criterion_target(),
            started=TaskController.current_task.get_start_date() <= datetime.now(),
            time=TaskController.current_task.get_allocated_time()
        )

        # Keep Assignment window up to date
        AssignmentController.refresh_tasks()

    @staticmethod
    def return_to_assignment():
        from Controller.AssignmentController import AssignmentController
        AssignmentController.assignment_window.root.deiconify()
        TaskController.task_window.root.destroy()

    @staticmethod
    def get_activities():
        from Controller.AssignmentController import AssignmentController
        assoc_tasks = []
        activities = []

        # For each assignment activity, get list of associated tasks
        counter = 0
        for item in AssignmentController.get_activities():
            assoc_tasks.append(ast.literal_eval(AssignmentController.get_activities()[counter][1]))
            counter += 1

        counter = 0
        for task_names in assoc_tasks:
            if TaskController.current_task.get_title() in task_names:
                activities.append([AssignmentController.get_activities()[counter][0],
                                   int(AssignmentController.get_activities()[counter][3]),
                                   False
                                   ])
            counter += 1

        return activities

    @staticmethod
    def update_activities():
        from Controller.AssignmentController import AssignmentController
        AssignmentController.current_assignment.update_activities()

        # Update the task window
        TaskController.task_window.populate_activities(TaskController.get_activities())