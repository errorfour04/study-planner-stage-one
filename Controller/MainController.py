import tkinter as tk

from Model.StudyProfile import StudyProfile
from Model.Module import Module

from Utilities.Datastore import Datastore
from View.MainWindow import MainWindow
from View.CreateStudyProfileWindow import CreateStudyProfileWindow

class MainController:
    study_profiles = Datastore.get_study_profiles()
    main_window = None
    create_study_profile_window = None
    data = None

    def __init__(self):
        pass

    @staticmethod
    def display_main_window():
        if len(MainController.study_profiles) < 1:
            MainController.main_window = MainWindow()
            MainController.main_window.display()
        else:
            MainController.init_data()
            MainController.main_window = MainWindow(MainController.data)
            MainController.main_window.display()

    @staticmethod
    def init_data():
        MainController.study_profiles = Datastore.get_study_profiles()

        # Get Module Information
        modules, cw_counts, ex_counts = [], [], []
        for profile in MainController.study_profiles:
            modules.append(
                [mod.get_name() for mod in profile.get_modules()]
            )

            cw_counts.append(
                [mod.get_num_coursework() for mod in profile.get_modules()]
            )

            ex_counts.append(
                [mod.get_num_exams() for mod in profile.get_modules()]
            )

        data = dict(
            profiles=[profile.get_title() for profile in MainController.study_profiles],
            modules=modules,
            num_coursework=cw_counts,
            num_exam=ex_counts
        )

        MainController.data = data


    @staticmethod
    def display_create_study_profile_window():
        MainController.create_study_profile_window = CreateStudyProfileWindow()
        MainController.create_study_profile_window.popup()

    @staticmethod
    def create_study_profile(title, semester_file):
        # Create Study Profile
        profile = StudyProfile(title)

        if profile.load_semester_file(semester_file):
            Datastore.add_profile(title)

            MainController.init_data()
            MainController.main_window.populate_profiles(MainController.data)
            MainController.create_study_profile_window.root.destroy()

        else:
            MainController.create_study_profile_window.display_error()

        try:
            MainController.main_window.populate_profiles(MainController.data)
            MainController.create_study_profile_window.destroy()
        except:
            MainController.main_window.profiles(MainController.data)

    @staticmethod
    def load_module(i, j):
        from Controller.ModuleController import ModuleController

        # Load appropriate module
        ModuleController.display_module_window(
            MainController.study_profiles[i].get_title(),
            MainController.study_profiles[i].get_modules()[j]
        )

        # Hide the current window
        MainController.main_window.hide()
