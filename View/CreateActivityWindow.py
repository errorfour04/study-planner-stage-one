from tkinter import *
from Controller.MilestoneController import MilestoneController
from Controller.ActivityController import ActivityController
from View.BaseWindow import BaseWindow
from Utilities.Datastore import Datastore
# from Model.StudyType import StudyType
from Model.Task import TaskType


import datetime


class CreateActivityWindow(BaseWindow):

    __MilestoneController = None
    __name_text_element = None
    __deadline_text_element = None
    __associated_tasks = None  # Checkbutton array
    __associated_tasks_check = None
    __tasks = None
    __study_type_set = None
    __study_type_check_set = None
    __value_text_element = None

    __info_frame = None
    __data_frame = None
    __study_frame = None

    def get_type(self):
        from Controller.TaskController import TaskController
        return TaskController.current_task.get_criterion()

    # constructor
    def __init__(self, assignment_name):
        self.__associated_tasks = []
        self.__associated_tasks_check = []
        self.__study_type_check_set = []
        self.__study_type_set = []
        self.__tasks = []

        # self.__MilestoneController = MilestoneController
        # self.__AssignmentController = AssignmentController
        super(CreateActivityWindow, self).__init__()

        # create the window
        self.root.title("Create Activity for " + assignment_name)
        self.root.geometry('{}x{}'.format(600, 500))
        self.position(600, 500)
        self.root.tk_setPalette("white")
        self.root.resizable(False, False)

        self.__ActivityController = ActivityController()
        self.__MilestoneController = MilestoneController()

        # info frame
        self.__info_frame = Frame(self.root, bg=self.theme['overlay'], width=100)
        self.__data_frame = Frame(self.root, bg=self.theme['overlay'], width=500)
        self.__study_frame = Frame(self.root, bg= self.theme['overlay'], width=100)

        # title
        title_label = Label(self.__info_frame, text="Create Activity", font='Times 20 bold',
                            bg=self.theme['overlay'])
        title_label.grid(row=0, column=5, columnspan=2, padx=10, pady=5, sticky=W)

        # NAME
        name_text_label = Label(self.__data_frame, bg=self.theme['overlay'],
                                text="Name of Activity: ", font=self.font['label'])
        name_text_label.grid(row=0, column=0, sticky=W)
        self.__name_text_element = Entry(self.__data_frame,
                                       bg=self.theme['overlay'],
                                       )
        # position NAME
        self.__name_text_element.grid(row=1, column=0, padx=10, pady=5, sticky=W)


        # TASKS
        chosen_type = str(self.get_type())

        tasks = Datastore.get_tasks(assignment_name)
        self.__tasks = tasks
        # print("taskcrit:",tasks[1].get_criterion(), "chose: ",chosen_type)
        # print(chosen_type == tasks[1].get_criterion())
        # for task in tasks:
        #     if task.get_criterion() == chosen_type:
        #         self.__tasks.append(task)

        for i in tasks:
            print(i.get_title(), " and type :",i.get_criterion())
        # tasks frame
        master_scroll_frame = Frame(self.root, bg=self.theme['overlay'])
        master_scroll_frame.grid(row=4, column=0, sticky=W)

        tasks_scroll = Scrollbar(master_scroll_frame)
        tasks_scroll.grid(row=0, column=1, sticky='nsew')

        scroll_canvas = Canvas(master_scroll_frame, width=400, height=100, bg=self.theme['overlay'],
                               yscrollcommand=tasks_scroll.set)
        scroll_canvas.grid(row=0, column=0, padx=5, sticky='nsew')

        tasks_scroll.config(command=scroll_canvas.yview)
        scroll_canvas.bind('<Configure>',
                           lambda e, canvas=scroll_canvas: canvas.config(scrollregion=canvas.bbox('all')))



        tasks_frame = Frame(scroll_canvas, bg=self.theme['overlay'], width=250)

        tasks_text_label = Label(self.__data_frame, bg=self.theme['overlay'],
                                 text="Tasks associated with Activity:", font=self.font['label'])
        tasks_text_label.grid(row=7, column=0, sticky=W)

        for i in tasks:
            but = Checkbutton(tasks_frame, text=i.get_title(), variable=tasks.index(i),
                              bg=self.theme['overlay'])
            but.grid(row=tasks.index(i) + 1, column=0, padx=5, sticky=W)
            self.__associated_tasks_check.append(but)

        task_window = scroll_canvas.create_window(0, 0, anchor='nw', window=tasks_frame)


        # study type

        study_types = [e.value for e in TaskType]
        study_type_label = Label(self.__study_frame, text="Study Type: ", bg=self.theme['overlay'], font=self.font['label'])
        study_type_label.grid(rows=len(tasks)+1, column=0, padx=5, sticky=W)

        master_scroll_frame_stud = Frame(self.root, bg=self.theme['overlay'])
        master_scroll_frame_stud.grid(row=6, column=0, sticky=W)

        tasks_scroll_stud = Scrollbar(master_scroll_frame_stud)
        tasks_scroll_stud.grid(row=0, column=1, sticky='nsew')

        scroll_canvas_stud = Canvas(master_scroll_frame_stud, width=400, height=100, bg=self.theme['overlay'],
                               yscrollcommand=tasks_scroll_stud.set)
        scroll_canvas_stud.grid(row=0, column=0, padx=5, sticky='nsew')

        tasks_scroll_stud.config(command=scroll_canvas_stud.yview)
        scroll_canvas_stud.bind('<Configure>',
                           lambda e, canvas=scroll_canvas_stud: canvas.config(scrollregion=canvas.bbox('all')))
        self.__study_frame = Frame(scroll_canvas_stud, bg=self.theme['overlay'])
        study_type_label = Label(self.__study_frame, text="Study types:", font=self.font['label'])
        study_type_label.grid(row=0, column=0, sticky=W)
        for i in study_types:
            # convert datetime to str
            but = Checkbutton(self.__study_frame, text=i, variable=len(tasks)+study_types.index(i),
                              bg=self.theme['overlay'])
            but.grid(rows=study_types.index(i) + 1, column=0, padx=5, sticky=W)
            self.__study_type_check_set.append(but)


        scroll_canvas_stud.create_window(0, 0, anchor='nw', window=self.__study_frame)

        value_text = Label(self.__data_frame, text="Contribution:", bg=self.theme['overlay'], font=self.font['label'])
        value_text.grid(row=4,column=0, sticky=W)
        self.__value_text_element = Entry(self.__data_frame, bg=self.theme['overlay'])
        self.__value_text_element.grid(row=5, column=0, sticky=W)

        self.__CreateMilestoneButton = Button(self.root, text="Submit", \
                                              command=lambda: self.create_activity_button_clicked(assignment_name),
                                              bg=self.theme['dull'])
        self.__CreateMilestoneButton.grid(row=6, column=1, sticky=W)

        self.__info_frame.grid(row=1, column=0, padx=10, pady=10, sticky='we')
        self.__data_frame.grid(row=2, column=0, padx=10, sticky='we')
        #self.__study_frame.grid(row=3, column=0, padx=10, pady=5, sticky='we')
        # tasks_frame.grid(row=5, column=0, sticky='we')


    def get_associated_tasks(self):
        associated_tasks = []
        for i in self.__AssociatedTasks:
            associated_tasks.append(self.__AssociatedTasks['text'])

        return associated_tasks

    def displayMessage(self, success):
        if success is True:
            self.__SuccessMessage['text'] = "Success!"
        else:
            self.__SuccessMessage['text'] = "Fail!"

    def check_before_task(self):
        count = 0

        for i in self.__associated_tasks_check:
            selected_deadline = self.__Tasks[count].get_end_date()
            input_deadline = datetime.datetime.strptime(self.__deadline_text_element.get(), "%Y-%m-%d %H:%M")
            if (int(i.getvar(str(count))) == 1 and
                    selected_deadline > input_deadline):
                return False

            count += 1

        return True

    def create_activity_button_clicked(self, assignment_name):
        name = self.__name_text_element.get()
        tasks_to_complete = []
        study_type = None
        value = self.__value_text_element.get()


        count = 0
        for i in self.__associated_tasks_check:
            if (int(i.getvar(str(count))) == 1):
                tasks_to_complete.append(i['text'])

            count += 1


        count = len(self.__associated_tasks_check)
        amount_selected = 0
        for i in self.__study_type_check_set:
            if (int(i.getvar(str(count))) == 1):
                study_type = i['text']
                amount_selected += 1

            count += 1

        # valid test conditions
        name_filled = True
        tasks_selected = True
        selected_one = True
        value_filled = True
        value_number = True

        print(amount_selected)
        if (value is ""):
            self.__SuccessMessage = Label(self.__data_frame, text="Please fill in Contribution field",
                                          bg=self.theme['overlay'], fg=self.theme['warning'])
            self.__SuccessMessage.grid(row=3, column=1, sticky=W)
            value_filled = False
        # validation
        elif (not (value.isnumeric())):
            self.__SuccessMessage = Label(self.__data_frame, text="Make sure contribution is a number",
                                          bg=self.theme['overlay'], fg=self.theme['warning'])
            self.__SuccessMessage.grid(row=3, column=1, sticky=W)
            value_number = False
        if not(amount_selected == 1):
            self.__SuccessMessage = Label(self.__study_frame, text="Select one Study Type",
                                          bg=self.theme['overlay'], fg=self.theme['warning'])
            self.__SuccessMessage.grid(row=1, column=1, sticky=W)
            selected_one = False






        if (name is ""):
            self.__SuccessMessage = Label(self.__data_frame, text="Please fill in Name field",
                                          bg=self.theme['overlay'], fg=self.theme['warning'])
            self.__SuccessMessage.grid(row=1, column=1, sticky=W)
            name_filled = False


        # tasks complete empty
        if (tasks_to_complete == []):
            self.__SuccessMessage = Label(self.__data_frame, text="Please select at least one task",
                                          bg=self.theme['overlay'], fg=self.theme['warning'])
            self.__SuccessMessage.grid(row=5, column=1, sticky=W)
            tasks_selected = False

        if (selected_one is True and name_filled is True and tasks_selected is True and
            value_filled is True and value_number is True):
            # add activity
            ActivityController().create_activity(name, tasks_to_complete, study_type, assignment_name, value)
            self.__SuccessMessage = Label(self.__data_frame, text="Success!", fg=self.theme['success'],
                                          bg=self.theme['overlay'])

            self.__SuccessMessage.grid(row=1, column=1, sticky=W)
            self.root.destroy()





