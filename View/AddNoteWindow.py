import tkinter as tk

from View.BaseWindow import BaseWindow

class AddNoteWindow(BaseWindow):
    def __init__(self):
        super(AddNoteWindow, self).__init__()

        # Basic Window Config
        self.root.title('Add Note')
        self.root.minsize(1,1)
        self.root.geometry('{}x{}'.format(400, 135))
        self.position(400, 135)
        self.root.tk_setPalette(self.theme['overlay'])
        self.root.resizable(False, False)
        self.root.bind('<Return>', lambda e: self.add_note_button_clicked())
        self.root.grid_columnconfigure(0, weight=1)
        self.root.grid_columnconfigure(1, weight=1)
        # self.root.bind('<Configure>', lambda e: self.note.focus_set())

        # Create Widgets
        self.note = tk.Text(
            self.root,
            height=3,
            bg=self.theme['overlay'],
            highlightcolor=self.theme['heading'],
            font='-size 14',
            wrap='word'
        )
        cancel_button = tk.Button(self.root, text='Cancel', command=self.cancel_button_clicked)
        add_note_button = tk.Button(
		self.root, text='Add Note',
		command=self.add_note_button_clicked, 
		default='active',
		bg=self.theme['heading'],
		fg=self.theme['overlay']
		)

        # Position widgets
        self.note.grid(row=0, columnspan=2, padx=10, pady=(20,5), sticky='ew')
        self.note.focus_set()
        cancel_button.grid(row=1, column=0, padx=10, pady=(5,20), sticky='ew')
        add_note_button.grid(row=1, column=1, padx=10, pady=(5,20), sticky='ew')

    def cancel_button_clicked(self):
        self.root.destroy()

    def add_note_button_clicked(self):
        from Controller.TaskController import TaskController
        text = str(self.note.get(1.0, 'end-1c')).strip()

        if (len(text) > 0) and (len(text) < 120):
            TaskController.add_note(text)
        else:
            print('Error')