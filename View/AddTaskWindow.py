import tkinter as tk
from tkinter import ttk
from datetime import datetime

from View.BaseWindow import BaseWindow

class AddTaskWindow(BaseWindow):
    def __init__(self, data):
        super(AddTaskWindow, self).__init__()
        criterion_options = ['Programming', 'Reading', 'Writing']
        self.dependencies = None

        # Basic Window Config
        self.root.title('Add New Task')
        self.root.minsize(1,1)
        self.root.geometry('450x325')
        self.position(450, 325)
        self.root.resizable(False, False)
        self.root.grid_columnconfigure(0, weight=1)
        self.root.grid_columnconfigure(1, weight=1)
        self.root.tk_setPalette(self.theme['overlay'])

        # Create Widgets
        self.description_label = tk.Label(self.root, text='Description', fg=self.theme['heading'])
        self.criterion_label = tk.Label(self.root, text='Type', fg=self.theme['heading'])
        self.quantity_label = tk.Label(self.root, text='Quantity', fg=self.theme['heading'])
        date_prompt = tk.Label(
            self.root,
            text='Date and time must be entered in the format DD/MM/YY hh:mm (24 hrs)',
            font='Arial 8 italic'
        )
        self.start_date_label = tk.Label(self.root, text='Start Date', fg=self.theme['heading'])
        self.end_date_label = tk.Label(self.root, text='End Date', fg=self.theme['heading'])
        self.dep_label = tk.Label(self.root, text='Add Dependency', fg=self.theme['heading'])
        self.dep_frame = tk.Frame(self.root)
        self.dep_frame.grid_columnconfigure(0, weight=1)

        self.description_entry = tk.Entry(self.root)
        self.criterion_select = ttk.Combobox(self.root, values=criterion_options, background=self.theme['overlay'])
        self.quantity_entry = tk.Entry(self.root)
        self.start_date_entry = tk.Entry(self.root)
        self.end_date_entry = tk.Entry(self.root)

        # List the other tasks, so user can add dependencies
        self.dep_scroll = tk.Scrollbar(self.dep_frame)
        self.dep_list_box = tk.Listbox(
            self.dep_frame,
            bg=self.theme['overlay'],
            selectbackground=self.theme['heading'],
            yscrollcommand=self.dep_scroll.set,
            height=4,
            selectmode='extended'
        )
        self.dep_list_box.bind(
            '<<ListboxSelect>>', lambda e, lb=self.dep_list_box: self.listbox_changed(lb.curselection())
        )

        for task in data['tasks']:
            self.dep_list_box.insert(data['tasks'].index(task), task)
        self.dep_scroll.configure(command=self.dep_list_box.yview)

        self.dep_list_box.grid(row=0, column=0, sticky='nsew')
        self.dep_scroll.grid(row=0, column=1, sticky='nsew')

        cancel_button = tk.Button(self.root, text='Cancel', command=self.cancel_button_clicked)
        add_task_button = tk.Button(
            self.root,
            text='Create Task',
            default='active',
            fg=self.theme['overlay'],
            bg=self.theme['heading'],
            command=self.add_task_button_clicked
        )

        # Position Widgets
        self.description_label.grid(row=0, column=0, padx=10, sticky='w')
        self.criterion_label.grid(row=2, column=0, padx=10, sticky='w')
        self.quantity_label.grid(row=2, column=1, padx=10, stick='w')
        date_prompt.grid(row=4, column=0, columnspan=2, pady=(10,0), padx=10, sticky='w')
        self.start_date_label.grid(row=5, column=0, padx=10, sticky='w')
        self.end_date_label.grid(row=5, column=1, padx=10, sticky='w')
        self.dep_label.grid(row=7, column=0, padx=10, sticky='w')

        self.description_entry.grid(row=1, column=0, columnspan=2, padx=10, sticky='we')
        self.criterion_select.grid(row=3, column=0, padx=10, sticky='ew')
        self.quantity_entry.grid(row=3, column=1, padx=10, sticky='ew')
        self.start_date_entry.grid(row=6, column=0, padx=10, sticky='ew')
        self.end_date_entry.grid(row=6, column=1, padx=10, sticky='ew')
        self.dep_frame.grid(row=8, column=0, columnspan=2, padx=13, pady=(0,10), sticky='ew')

        add_task_button.grid(row=9, column=0, columnspan=2, padx=10, sticky='ew')
        cancel_button.grid(row=10, column=0, columnspan=2, padx=10, sticky='ew')

        self.description_entry.focus_set()
        # Submit new task by pressing the enter button
        self.root.bind('<Return>', lambda e: self.add_task_button_clicked())

    def cancel_button_clicked(self):
        self.root.destroy()

    def add_task_button_clicked(self):
        from Controller.AssignmentController import AssignmentController
        errors = []

        self.description_entry.config(bg=self.theme['overlay'])
        self.criterion_select.config(background=self.theme['overlay'])
        self.quantity_entry.config(bg=self.theme['overlay'])
        self.start_date_entry.config(bg=self.theme['overlay'])
        self.end_date_entry.config(bg=self.theme['overlay'])

        try:
            quantity = int(self.quantity_entry.get())
            if int(self.quantity_entry.get()) < 1:
                errors.append('quantity')
        except:
            errors.append('quantity')
            
        start_date = ''
        end_date = ''

        # Set default time
        if len(str(self.start_date_entry.get()).split(' ')) == 1:
            start_date = self.start_date_entry.get() + ' 15:00'
        else:
            # self.start_date_entry.config(state='normal')
            start_date = self.start_date_entry.get()

        if len(str(self.end_date_entry.get()).split(' ')) == 1:
            end_date = self.end_date_entry.get() + ' 15:00'
        else:
            end_date = self.end_date_entry.get()

        # Fill Errors
        if len(str(self.description_entry.get()).strip()) < 1:
            errors.append('description')

        if len(self.criterion_select.get()) < 1:
            errors.append('criterion')

        if len(str(self.start_date_entry.get()).strip()) < 1:
            errors.append('start date')

        if len(str(self.end_date_entry.get()).strip()) < 1:
            errors.append('end date')

        try:
            datetime.strptime(start_date, '%d/%m/%y %H:%M')
        except:
            errors.append('start date')

        try:
            datetime.strptime(end_date, '%d/%m/%y %H:%M')
        except:
            errors.append('end date')

        if len(errors) == 0:
            AssignmentController.add_task(
                self.description_entry.get(),
                self.criterion_select.get(),
                start_date,
                end_date,
                quantity,
                self.dependencies
            )
        else:
            if 'description' in errors:
                self.description_entry.config(bg='pink')
            if 'criterion' in errors:
                self.criterion_select.config(background=self.theme['overlay'])
            if 'quantity' in errors:
                self.quantity_entry.config(bg='pink')
            if 'start date' in errors:
                self.start_date_entry.config(bg='pink')
            if 'end date' in errors:
                self.end_date_entry.config(bg='pink')

    def listbox_changed(self, dependencies):
        self.dependencies = dependencies
        from Controller.AssignmentController import AssignmentController
        AssignmentController.manage_start_date(dependencies)

    def set_start_date(self, date):
        if date is not None:
            self.start_date_entry.config(state='normal')
            self.start_date_entry.delete(0, tk.END)
            self.start_date_entry.insert(0, date)
            self.start_date_entry.config(state='disabled')
        else:
            self.start_date_entry.config(state='normal')