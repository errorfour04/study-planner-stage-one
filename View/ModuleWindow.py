import tkinter as tk

from Model.Module import Module
from View.BaseWindow import BaseWindow

class ModuleWindow(BaseWindow):
    def __init__(self, data):
        super(ModuleWindow, self).__init__()

        self.root.title('{} - {}'.format(data['profile'], data['moduletitle']))
        self.root.minsize(1000, 650)
        self.root.geometry('1000x650')
        self.position(1000, 650)
        self.root.grid_columnconfigure(0, weight=1)

        #Back Button
        back_button = tk.Button(self.root, text='Return to Study Dashboard', command=self.back_button_clicked)
        back_button.grid(row=0, padx=10, pady=(10, 5), sticky='w')

        modules_title = tk.Label(self.root, text=data['moduletitle'], font='Arial 20 bold')
        module_frame = tk.Frame(self.root, background=self.theme['overlay'])
        module_frame.grid_columnconfigure(0, weight=1)
        module_frame.grid_columnconfigure(1, weight=1)

        # Inside Task Frame
        module_frame_heading = tk.Label(
            module_frame,
            text='All Assignments',
            font='Arial 16 bold',
            fg=self.theme['heading'],
            bg=self.theme['overlay']
        )

        module_table_heading1 = tk.Label(
            module_frame, text='Name', font='Arial 10 bold', bg=self.theme['overlay']
        )
        module_table_heading2 = tk.Label(
            module_frame, text='Type', font='Arial 10 bold', bg=self.theme['overlay']
        )

        module_scroll = tk.Scrollbar(module_frame)
        module_canvas = tk.Canvas(
            module_frame,
            bg=self.theme['overlay'],
            bd=-5,
            yscrollcommand=module_scroll.set
        )
        module_scroll.config(command=module_canvas.yview)
        module_frame.bind(
            '<Configure>', lambda e, canvas=module_canvas: canvas.config(scrollregion=canvas.bbox('all'))
        )

        # Inside Canvas
        self.canvas_frame = tk.Frame(module_canvas, bg=self.theme['overlay'])
        self.canvas_frame.grid_columnconfigure(0, weight=1)
        self.canvas_frame.grid_columnconfigure(1, weight=1)

        # Position widgets inside module Canvas
        modules_frame_window = module_canvas.create_window(5, 5, anchor='nw', window=self.canvas_frame)
        module_canvas.itemconfig(modules_frame_window, width=module_canvas.winfo_reqwidth() * 2.54)

        # Position widgets inside module Frame
        module_frame_heading.grid(row=0, columnspan=3, padx=10, pady=5, sticky='w')
        module_table_heading1.grid(row=1, column=0, padx=5, pady=5, sticky='w')
        module_table_heading2.grid(row=1, column=1, pady=5, sticky='w')
        module_canvas.grid(row=2, columnspan=3, padx=(10, 0), pady=(0, 5), sticky='nsew')
        # task_canvas.config(scrollregion=task_canvas.bbox('all'))

        modules_title.grid(row=2, padx=10, pady=10, sticky='w')
        module_frame.grid(row=4, padx=10, pady=(10, 0), sticky='news')
        # add_assignment_button.grid(row=5, columnspan=4, padx=10, sticky='nsew')

        self.populate_assignments(data['assignments'], data['types'])

    def populate_assignments(self, assignments, types):
        counter = 0
        for assignment in assignments:
            assignment_name = tk.Label(self.canvas_frame, text=assignment, bg=self.theme['overlay'])
            assignment_name.grid(row=counter, column=0, padx=5, pady=5, sticky='w')
            assignment_name.bind('<Button-1>', lambda e, index=counter: self.assignment_row_clicked(index))
            counter +=1

        counter = 0

        for type in types:
            type_label = tk.Label(self.canvas_frame, text='({})'.format(type), bg=self.theme['overlay'])
            type_label.grid(row=counter, column=1, padx=(0, 0), pady=5, sticky='w')
            type_label.bind('<Button-1>', lambda e, index=counter: self.assignment_row_clicked(index))
            counter += 1


    def assignment_row_clicked(self, index):
        from Controller.ModuleController import ModuleController
        ModuleController.load_assignment(index)

    def back_button_clicked(self):
        from Controller.MainController import MainController
        MainController.main_window.show()

        self.root.winfo_children().clear()
        self.root.destroy()

