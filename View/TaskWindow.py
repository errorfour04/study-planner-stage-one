import tkinter as tk
from tkinter import messagebox

from View.BaseWindow import BaseWindow


class TaskWindow(BaseWindow):
    def __init__(self, data):
        super(TaskWindow, self).__init__()
        self.data = data
        self.root.title('{} - {}'.format(data['assignment'], data['title']))
        self.root.geometry('{}x{}'.format(1000, 675))
        self.position(1000, 675)
        self.root.resizable(False, False)
        self.root.grid_columnconfigure(0, weight=2)
        self.root.grid_columnconfigure(1, weight=3)

        self.note_message = None
        self.dep_message = None

        # Add back button
        back_button = tk.Button(self.root, text='Return to Assignment Page', command=self.back_button_clicked)
        back_button.grid(row=0, column=0, padx=10, pady=10, sticky='w')

        # Add complete task button
        self.complete_task_button = tk.Button(
            self.root,
			text='Mark Task as Complete', 
			command=self.complete_task_button_clicked, 
			bg=self.theme['heading'],
			fg=self.theme['overlay'],
			default='active'
        )
        self.complete_task_button.grid(row=0, column=1, padx=10, pady=10, sticky='e')
        self.complete_task_button.configure(state='disabled')

        # Create Frames
        self.info_frame = tk.Frame(self.root, bg=self.theme['overlay'], width=100)
        dep_frame = tk.Frame(self.root, bg=self.theme['overlay'])
        note_frame = tk.Frame(self.root, bg=self.theme['overlay'])
        self.activity_frame = tk.Frame(self.root, bg=self.theme['overlay'], height=500)

        # Inside Info Frame
        title_label = tk.Label(self.info_frame, text=data['title'], font='Arial 20 bold', bg=self.theme['overlay'])

        criterion_label = tk.Label(self.info_frame, text=data['criterion'], bg=self.theme['overlay'])
        start_date_label = tk.Label(self.info_frame, text='Start Date', bg=self.theme['overlay'])
        end_date_label = tk.Label(self.info_frame, text='End Date', bg=self.theme['overlay'])
        time_label = tk.Label(self.info_frame, text='Time Spent', bg=self.theme['overlay'])

        self.criterion_val_label = tk.Label(
            self.info_frame,
            text='{} / {}'.format(data['amt_completed'], data['target']),
            bg=self.theme['overlay'],
            fg=self.theme['heading'],
            font='Arial 10 bold'
        )
        start_date_val_label = tk.Label(
            self.info_frame,
            text=data['start'],
            bg=self.theme['overlay'],
            fg=self.theme['heading'],
            font='Arial 10 bold'
        )
        end_date_val_label = tk.Label(
            self.info_frame,
            text=data['end'],
            bg=self.theme['overlay'],
            fg=self.theme['heading'],
            font='Arial 10 bold'
        )
        self.time_val_label = tk.Label(
            self.info_frame,
            text='{} hours'.format(data['time']),
            bg=self.theme['overlay'],
            fg=self.theme['heading'],
            font='Arial 10 bold'
        )

        # Position Inside Info Frame
        title_label.grid(row=0, column=0, columnspan=2, padx=10, pady=5, sticky='w')
        self.create_status_label(data['completed'], data['started'], data['ended'])
        criterion_label.grid(row=2, column=0, padx=10, pady=5, sticky='w')
        start_date_label.grid(row=3, column=0, padx=10, pady=5, sticky='w')
        end_date_label.grid(row=4, column=0, padx=10, pady=5, sticky='w')
        time_label.grid(row=5, column=0, padx=10, pady=5, sticky='w')

        self.criterion_val_label.grid(row=2, column=1, padx=10, pady=5, sticky='e')
        start_date_val_label.grid(row=3, column=1, padx=10, pady=5, sticky='e')
        end_date_val_label.grid(row=4, column=1, padx=10, pady=5, sticky='e')
        self.time_val_label.grid(row=5, column=1, padx=10, pady=5, sticky='e')

        # Inside Dep Frame
        dep_heading = tk.Label(
            dep_frame, text='Dependencies', font='Arial 16 bold', bg=self.theme['overlay'], fg=self.theme['heading']
        )
        dep_scroll = tk.Scrollbar(dep_frame)
        dep_canvas = tk.Canvas(
            dep_frame,
            bg=self.theme['overlay'],
            bd=-5,
            yscrollcommand=dep_scroll.set,
            height=100
        )
        dep_scroll.config(command=dep_canvas.yview)
        dep_frame.bind('<Configure>', lambda e, canvas=dep_canvas: canvas.config(scrollregion=canvas.bbox('all')))

        # Inside Dep Canvas
        self.dep_canvas_frame = tk.Frame(dep_canvas, bg=self.theme['overlay'])
        if len(data['dep_title']) < 1:
            self.dep_message = tk.Label(
                self.dep_canvas_frame, text='This task is not dependent on any others.', bg=self.theme['overlay']
            )
            self.dep_message.grid(row=0, columnspan=2, sticky='w')
        else:
            self.populate_dependencies(data['dep_title'])

        # Position widgets inside Dep Canvas
        dep_frame_window = dep_canvas.create_window(12, 0, anchor='nw', window=self.dep_canvas_frame)

        # Position Inside Dep Frame
        dep_heading.grid(row=0, column=0, columnspan=2, padx=10, pady=5, sticky='w')
        dep_canvas.grid(row=1, columnspan=2, padx=(10, 0), pady=(0, 5), sticky='nsew')
        dep_scroll.grid(row=1, column=2, padx=(0, 10), pady=(0, 5), sticky='nsew')

        # Inside Note Frame
        note_heading = tk.Label(
            note_frame, text='Notes', font='Arial 16 bold', bg=self.theme['overlay'], fg=self.theme['heading']
        )
        add_note_button = tk.Label(
            note_frame, text='+', font='Calibri 25 bold', fg=self.theme['success'], bg=self.theme['overlay']
        )
        note_scroll = tk.Scrollbar(note_frame)
        note_canvas = tk.Canvas(
            note_frame,
            bg=self.theme['overlay'],
            bd=-5,
            yscrollcommand=note_scroll.set,
            height=100
        )
        note_scroll.config(command=note_canvas.yview)
        note_frame.bind('<Configure>', lambda e, canvas=note_canvas: canvas.config(scrollregion=canvas.bbox('all')))
        # note_frame.bind('<Enter>', lambda e, canvas=note_canvas: canvas.config(scrollregion=canvas.bbox('all')))

        # Inside Note Canvas
        self.note_canvas_frame = tk.Frame(note_canvas, bg=self.theme['overlay'])
        if len(data['notes']) < 1:
            self.note_message = tk.Label(
                self.note_canvas_frame, text='You have not added any notes to this task.', bg=self.theme['overlay']
            )
            self.note_message.grid(row=0, columnspan=2, sticky='w')
        else:
            self.populate_notes(data['notes'])

        # Position widgets inside Note Canvas
        note_frame_window = note_canvas.create_window(12, 0, anchor='nw', window=self.note_canvas_frame)

        # Position Inside Note Frame
        note_heading.grid(row=0, column=0, padx=10, pady=5, sticky='w')
        add_note_button.grid(row=0, column=1, columnspan=2, padx=10, sticky='e')
        add_note_button.bind('<Button-1>', lambda e: self.add_note_button_clicked())
        note_canvas.grid(row=1, columnspan=2, padx=(10, 0), pady=(0, 5), sticky='nsew')
        note_scroll.grid(row=1, column=2, padx=(0, 10), pady=(0, 5), sticky='nsew')

        # Inside Activity Frame
        activity_heading = tk.Label(
            self.activity_frame,
            text='Study Activities',
            font='Arial 20 bold',
            bg=self.theme['overlay'],
            fg=self.theme['heading']
        )
        add_activity_button = tk.Label(
            self.activity_frame, text='+', font='Calibri 25 bold', fg=self.theme['success'], bg=self.theme['overlay']
        )
        activity_scroll = tk.Scrollbar(self.activity_frame)
        activity_canvas = tk.Canvas(
            self.activity_frame,
            bg=self.theme['overlay'],
            bd=-5,
            yscrollcommand=activity_scroll.set,
            height=475
        )
        activity_scroll.config(command=activity_canvas.yview)
        self.activity_frame.bind(
            '<Configure>', lambda e, canvas=activity_canvas: canvas.config(scrollregion=canvas.bbox('all'))
        )

        # Inside Activity Canvas
        self.activity_canvas_frame = tk.Frame(activity_canvas, bg=self.theme['overlay'])
        self.activity_canvas_frame.grid_columnconfigure(0, weight=1)


        if len(data['activities']) < 1:
            self.activity_message = tk.Label(
                self.activity_canvas_frame,
                text='Add activities to contribute to task completion',
                bg=self.theme['overlay']
            )
            self.activity_message.grid(row=0, columnspan=2, sticky='w')
        else:
            self.populate_activities(data['activities'])

        # Position widgets inside Note Canvas
        activity_frame_window = activity_canvas.create_window(12, 0, anchor='nw', window=self.activity_canvas_frame)
        activity_canvas.itemconfig(activity_frame_window, width=activity_canvas.winfo_reqwidth() * 1.2)

        # Position Inside Activity Frame
        activity_heading.grid(row=0, column=0, padx=10, pady=5, sticky='w')
        add_activity_button.grid(row=0, column=1, columnspan=2, padx=10, sticky='e')
        add_activity_button.bind('<Button-1>', lambda e: self.add_activity_button_clicked())
        activity_canvas.grid(row=1, columnspan=2, padx=(10, 0), pady=(0, 5), sticky='nsew')
        activity_scroll.grid(row=1, column=2, padx=(0, 10), pady=(0, 5), sticky='nsew')

        # Position Frames
        self.info_frame.grid(row=1, column=0, padx=10, pady=10, sticky='we')
        dep_frame.grid(row=2, column=0, padx=10, pady=10, sticky='we')
        note_frame.grid(row=3, column=0, padx=10, pady=10, sticky='we')
        self.activity_frame.grid(row=1, rowspan=4, column=1, padx=10, pady=10, sticky='we')


        self.info_frame.grid_columnconfigure(0, weight=1)
        self.info_frame.grid_columnconfigure(1, weight=1)
        dep_frame.grid_columnconfigure(0, weight=1)
        dep_frame.grid_columnconfigure(1, weight=1)
        note_frame.grid_columnconfigure(0, weight=1)
        note_frame.grid_columnconfigure(1, weight=1)
        self.activity_frame.grid_columnconfigure(0, weight=1)
        self.activity_frame.grid_columnconfigure(1, weight=1)

        # Determines whether complete task button should be enabled
        self.update_info(data['completed'], data['started'], data['amt_completed'], data['target'])
        self.populate_activities(data['activities'])

    def create_status_label(self, complete, started, ended):
        if complete:
            self.status_label = tk.Label(
                self.info_frame, text='Completed', bg=self.theme['overlay'], fg=self.theme['success']
            )
        elif ended:
            self.status_label = tk.Label(
                self.info_frame, text='Overdue', bg=self.theme['overlay'], fg=self.theme['danger']
            )
        elif started:
            self.status_label = tk.Label(
                self.info_frame, text='Ongoing', bg=self.theme['overlay'], fg=self.theme['warning']
            )
        else:
            self.status_label = tk.Label(
                self.info_frame, text='Not Started', bg=self.theme['overlay'], fg=self.theme['dull']
            )

        self.status_label.grid(row=1, column=0, padx=10, pady=5, sticky='w')

    def populate_dependencies(self, dependencies):
        # Remove dependency message
        if self.dep_message:
            self.dep_message.destroy()

        counter = 0

        for dependency in dependencies:
            dep_text = tk.Label(self.dep_canvas_frame, text=dependency, bg=self.theme['overlay'])
            dep_text.grid(row=counter, columnspan=2, sticky='w')

            counter += 1

    def populate_notes(self, notes):
        # Remove note message
        if self.note_message:
            self.note_message.destroy()

        counter = 0

        for note in notes:
            note_text = tk.Label(self.note_canvas_frame, text='\u2022 {}'.format(note), bg=self.theme['overlay'])
            note_text.grid(row=counter, columnspan=2, sticky='w')

            counter += 1

    def populate_activities(self, activities):
        counter = 0
        if len(activities) > 0:
            for child in self.activity_canvas_frame.winfo_children():
                child.destroy()

        for activity in activities:
            activity_title = tk.Label(self.activity_canvas_frame, text=activity[0], bg=self.theme['overlay'])
            activity_amount = tk.Label(self.activity_canvas_frame, text=activity[1], bg=self.theme['overlay'])
            complete_activity_button = tk.Button(self.activity_canvas_frame, text='Complete', default='active')

            # Conditionally disable the button and change colour
            if activity[2] == True:
                activity_title.config(fg=self.theme['success'])
                activity_amount.config(fg=self.theme['success'])
                complete_activity_button.config(state='disabled')
            else:
                complete_activity_button.bind(
                    '<Button-1>', lambda e, index=counter: self.complete_activity_button_clicked(index)
                )

            # Positioning
            activity_title.grid(row=counter, column=0, padx=5, pady=10, sticky='w')
            activity_amount.grid(row=counter, column=1, padx=(0,5), pady=10, sticky='e')
            complete_activity_button.grid(row=counter, column=2, pady=10, sticky='e')


            counter += 1

    def update_info(self, completed, started=None, amt_completed=None, target=None, time=None):
        # Disable the complete task button after it's clicked
        if completed:
            self.complete_task_button.configure(state='disabled')
            self.status_label.config(text='Completed', fg=self.theme['success'])
        # Update the data on screen after activities are completed
        if amt_completed is not None and target is not None and time is not None:
            self.criterion_val_label.config(text='{} / {}'.format(amt_completed, target))
            self.time_val_label.config(text='{} hours'.format(time))
        # Enable the complete task button if student has done the necessary work & task isn't already complete
        if not completed and amt_completed == target and started:
            self.complete_task_button.configure(state='active')

    def add_note_button_clicked(self):
        from Controller.TaskController import TaskController
        TaskController.display_add_note_window()

    def add_activity_button_clicked(self):
        from Controller.TaskController import TaskController
        TaskController.add_activity()

    def complete_activity_button_clicked(self, index):
        from Controller.TaskController import TaskController
        TaskController.display_activity_time_window(index)

    def display_add_activity_error(self):
        messagebox.showinfo('Error', "This task has incomplete tasks that it is dependent on.")

    def back_button_clicked(self):
        from Controller.TaskController import TaskController
        TaskController.return_to_assignment()

    def complete_task_button_clicked(self):
        from Controller.TaskController import TaskController
        TaskController.complete_task()
