import tkinter as tk
from tkinter import filedialog

from View.BaseWindow import BaseWindow

class MainWindow(BaseWindow):
    def __init__(self, data=None):
        super(MainWindow, self).__init__()
        # self.data = data

        self.root.title('Study Planner')
        self.root.geometry('{}x{}'.format(800, 420))
        self.position(800, 420)
        self.root.resizable(False, False)
        self.root.grid_columnconfigure(0, weight=1)
        self.root.grid_columnconfigure(2, weight=1)

        # Create Widgets
        heading = tk.Label(self.root, text='Welcome Student', font='Arial 30')
        self.info_frame = tk.Frame(self.root)
        self.info_frame.grid_columnconfigure(0, weight=1)
        self.info_frame.grid_columnconfigure(2, weight=1)

        # Position Widgets
        heading.grid(row=0, column=1, padx=10, pady=10, sticky='w')
        self.info_frame.grid(row=1, columnspan=3, padx=10, pady=10, sticky='ew')
        if not data:
            self.no_profiles()
        else:
            self.profiles(data)

    # No Study Profiles Exist
    def no_profiles(self):
        # Create Widgets
        message = tk.Label(
            self.info_frame, text="You don't have any study profiles yet.\nCreate one now.", fg=self.theme['dull']
        )
        create_study_profile_button = tk.Button(
            self.info_frame,
            text='Create Study Profile',
            default='active',
            command=self.create_study_profile_button_clicked,
            bg=self.theme['heading'],
            fg=self.theme['overlay']
        )

        # Position Widgets
        message.grid(row=0, columnspan=3, padx=10, pady=(75,5), sticky='ew')
        create_study_profile_button.grid(row=1, column=1, padx=10, sticky='nsew')

    # Study Profiles Exist
    def profiles(self, data):
        # Destroy existing widgets
        if not len(self.info_frame.winfo_children()) == 0:
            for child in self.info_frame.winfo_children():
                child.destroy()

        # Create Widgets
        heading = tk.Label(self.info_frame, text='Semester Study Profiles', font='Arial 20 bold')
        main_canvas_scroll = tk.Scrollbar(self.info_frame)
        main_canvas = tk.Canvas(
            self.info_frame,
            bg=self.theme['overlay'],
            bd=-5,
            yscrollcommand=main_canvas_scroll.set,
            width=725,
            height=250
        )
        main_canvas_scroll.config(command=main_canvas.yview)
        main_canvas.bind(
            '<Configure>', lambda e, canvas=main_canvas: canvas.config(scrollregion=canvas.bbox('all'))
        )

        self.main_canvas_frame = tk.Frame(main_canvas, bg=self.theme['overlay'])
        self.main_canvas_frame.grid_columnconfigure(0, weight=1)
        main_canvas.create_window(10, 0, anchor='nw', window=self.main_canvas_frame)

        self.populate_profiles(data)

        create_study_profile_button = tk.Button(
            self.info_frame,
            text='Create Study Profile',
            default='active',
            command=self.create_study_profile_button_clicked,
            bg=self.theme['heading'],
            fg=self.theme['overlay']
        )


        # Position Widgets
        heading.grid(row=0, columnspan=2, padx=10, pady=5, sticky='w')
        main_canvas.grid(row=1, column=0, padx=(15,0), pady=5, sticky='nsew')
        main_canvas_scroll.grid(row=1, column=1, pady=5, sticky='nsew')
        create_study_profile_button.grid(row=2, columnspan=2, padx=(10,0), sticky='ew')

    def populate_profiles(self, data):
        counter = 0

        for profile in data['profiles']:
            # Create Widgets
            inner_frame = tk.Frame(self.main_canvas_frame, bg=self.theme['overlay'], width=400)
            title = tk.Label(
                inner_frame, text=profile, font='Arial 20 bold', bg=self.theme['overlay'], fg=self.theme['heading']
            )
            inner_canvas_scroll = tk.Scrollbar(inner_frame)
            inner_canvas = tk.Canvas(
                inner_frame,
                yscrollcommand=inner_canvas_scroll.set,
                bg=self.theme['overlay'],
                bd=-5,
                height=80,
                width=725
            )
            inner_canvas_scroll.config(command=inner_canvas.yview)
            inner_canvas.bind(
                '<Configure>', lambda e, canvas=inner_canvas: canvas.config(scrollregion=canvas.bbox('all'))
            )

            # Inside the inner canvas
            module_frame = tk.Frame(inner_canvas, bg=self.theme['overlay'])
            module_frame.grid_columnconfigure(0, weight=1)
            # inner_canvas.grid_columnconfigure(0, weight=1)

            # Create Widgets for each module
            inner_counter = 0
            for module in data['modules'][counter]:
                module_title = tk.Label(module_frame, text=module, bg=self.theme['overlay'])
                num_exam = tk.Label(
                    module_frame,
                    text='Coursework:{}'.format(data['num_coursework'][counter][inner_counter]),
                    bg=self.theme['overlay'],
                    fg=self.theme['dull']
                )
                num_coursework = tk.Label(
                    module_frame,
                    text='Exam:{}'.format(data['num_exam'][counter][inner_counter]),
                    bg=self.theme['overlay'],
                    fg=self.theme['dull']
                )

                module_title.grid(row=inner_counter, column=0, padx=10, pady=(0,5), sticky='w')
                num_exam.grid(row=inner_counter, column=1, padx=10, pady=(0,5), sticky='w')
                num_coursework.grid(row=inner_counter, column=2, padx=10, pady=(0,5), sticky='w')

                module_title.bind('<Button-1>', lambda e, i=counter, j=inner_counter: self.module_clicked(i, j))
                num_exam.bind('<Button-1>', lambda e, i=counter, j=inner_counter: self.module_clicked(i, j))
                num_coursework.bind('<Button-1>', lambda e, i=counter, j=inner_counter: self.module_clicked(i, j))

                inner_counter += 1


            # Position module frame in canvas
            module_frame_window = inner_canvas.create_window(0, 0, anchor='nw', window=module_frame)
            inner_canvas.itemconfig(module_frame_window, width=inner_canvas.winfo_reqwidth())

            title.grid(row=0, column=0, sticky='w')
            inner_canvas.grid(row=1, column=0, sticky='nsew')
            inner_canvas_scroll.grid(row=1, column=1, sticky='nsew')

            # Position Widgets
            inner_frame.grid(row=counter, column=0, pady=5, sticky='ew')

            counter += 1

    def create_study_profile_button_clicked(self):
        from Controller.MainController import MainController
        MainController.display_create_study_profile_window()

    def module_clicked(self, i, j):
        from Controller.MainController import MainController
        MainController.load_module(i, j)