from tkinter import *
from Controller.MilestoneController import MilestoneController
from View.BaseWindow import BaseWindow
from Utilities.Datastore import Datastore

class ViewMilestoneWindow(BaseWindow):


    # window
    __view_milestone_window = None
    __view_task_window = None


    # value attributes
    __name_text = ""
    __deadline_text = ""
    __tasks_to_complete = None
    __complete = False
    __progress_text = ""

    # gui elements
    __complete_button = None
    __back_button = None
    __name_element = None
    __deadline_element = None
    __tasks_to_complete_check = None
    __complete_element = None
    __progress_element = None
    __button_pressed = None

    # Controller
    __milestone_controller = None


    def __init__(self, milestone_name):
        super(ViewMilestoneWindow, self).__init__()
        self.__milestone_controller = MilestoneController()
        self.root.geometry('{}x{}'.format(500, 600))
        self.root.title(milestone_name)
        self.position(500, 600)
        self.__tasks_to_complete = []
        self.__button_pressed = 0

        #info frame
        self.__info_frame = Frame(self.root, bg=self.theme['overlay'], width=100)
        self.__data_frame = Frame(self.root, bg=self.theme['overlay'], width=500)


        # text contents
        m_obj = self.__milestone_controller.get_milestone(milestone_name)

        self.__name_text = m_obj.get_name()
        self.__deadline_text = m_obj.get_deadline().strftime("%Y-%m-%d %H:%M")

        # loading the tasks to complete
        self.__tasks_to_complete = m_obj.get_tasks_to_complete()
        # print(tmp_tasks_to_complete)
        # for i in tmp_tasks_to_complete:
        #     self.__tasks_to_complete.append(Datastore.get_task(i))

        self.__complete = self.__milestone_controller.get_milestone(milestone_name).get_complete()
        self.__progress_text = '{0:.1f}'.format(self.__milestone_controller.get_milestone(milestone_name).get_progress())


        # name
        name = Label(self.__info_frame, text="Milestone", bg=self.theme['overlay'], font=self.font['label'])
        name.grid(row=0, column=0,sticky=W)
        self.__name_element = Label(self.__info_frame, text=self.__name_text,bg=self.theme['overlay'])
        self.__name_element.grid(row=1, column=0, padx=5, sticky=W)

        # deadline
        deadline=Label(self.__info_frame, text="Deadline", bg=self.theme['overlay'], font=self.font['label'])
        deadline.grid(row=2, column=0,sticky=W)
        self.__deadline_element = Label(self.__info_frame, text=self.__deadline_text, bg="white")
        self.__deadline_element.grid(row=3, column=0, padx=5, sticky=W)

        if(self.__complete):
            self.__complete_element = Label(self.__info_frame, text="Complete: " + self.__progress_text + "%",
                                            font=self.font['label'], bg=self.theme['overlay'], fg=self.theme['success'])

        else:
            self.__complete_element = Label(self.__info_frame, text="Incomplete: " + self.__progress_text + "%",
                                            font=self.font['label'], bg=self.theme['overlay'], fg=self.theme['warning'])

        self.__complete_element.grid(row=4, column=0, sticky=W)

        self.__tasks_to_complete_check = []
        tasks_label = Label(self.__data_frame, text="Associated Tasks: ", bg=self.theme['overlay'],
                            font=self.font['label'])
        tasks_label.grid(row=1, column=0, sticky=W)
        # tasks to complete

        master_scroll_frame = Frame(self.root, bg=self.theme['overlay'])
        master_scroll_frame.grid(row=4, column=0, sticky=W)

        tasks_scroll = Scrollbar(master_scroll_frame)
        tasks_scroll.grid(row=0, column=1, sticky='nsew')

        scroll_canvas = Canvas(master_scroll_frame, width=400, height=100, bg=self.theme['overlay'],
                               yscrollcommand=tasks_scroll.set)
        scroll_canvas.grid(row=0, column=0, padx=5, sticky='nsew')

        tasks_scroll.config(command=scroll_canvas.yview)
        scroll_canvas.bind('<Configure>',
                           lambda e, canvas=scroll_canvas: canvas.config(scrollregion=canvas.bbox('all')))

        tasks_frame = Frame(scroll_canvas, bg=self.theme['overlay'], width=250)
        for i in self.__tasks_to_complete:
            check_height = 3 + self.__tasks_to_complete.index(i)
            but = Button(tasks_frame, text = i.get_title())
            but.grid(row=check_height, column = 0, sticky=W)
            but.bind('<Button-1>', (lambda e, index=self.__tasks_to_complete.index(i): self.view_task(index)))
            self.__tasks_to_complete_check.append(i)

        # complete status + progress

        scroll_canvas.create_window(0, 0, anchor='nw', window=tasks_frame)

        # back button
        self.__back_button = Button(self.root, text="Back")
        self.__back_button.grid(row=check_height+3, column=0, sticky=W)
        self.__back_button.bind('<Button-1>', (lambda e: self.back(milestone_name)))

        self.__info_frame.grid(row=1, column=0, padx=10, pady=10, sticky='we')
        self.__data_frame.grid(row=2, column=0, padx=10, pady=10, sticky='we')

    def back(self, milestone_name):
        from Controller.AssignmentController import AssignmentController

        # to_go_back_to = MilestoneController().get_assignment(milestone_name)
        # AssignmentController.display_assignment_window("mod", to_go_back_to)
        AssignmentController.assignment_window.show()
        self.root.winfo_children().clear()
        self.root.destroy()

    def view_task(self, index):
        from Controller.TaskController import TaskController

        print(self.__tasks_to_complete_check[index])
        TaskController.display_task_window("A", self.__tasks_to_complete_check[index])










