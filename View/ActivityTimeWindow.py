from View.BaseWindow import BaseWindow
from Controller.ActivityController import ActivityController
from tkinter import *
class ActivityTimeWindow(BaseWindow):

    __time_spent = None
    __info_frame = None

    def __init__(self, activity_name):
        super(ActivityTimeWindow, self).__init__()
        self.root.geometry('{}x{}'.format(300, 100))

        self.__info_frame = Frame(self.root, bg=self.theme['overlay'], width=200)
        self.position(300, 100)
        time_text = Label(self.__info_frame, text="Hours spent: ", bg=self.theme['overlay'])
        time_text.grid(row=1, column=0, sticky=W)

        self.__time_spent = Entry(self.__info_frame, bg=self.theme['overlay'])
        self.__time_spent.grid(row=2, column=0, padx=5, sticky=W)

        submit = Button(self.__info_frame, text="Submit")
        submit.grid(row=3, column=1, sticky=W)
        submit.bind('<Button-1>', (lambda e: self.set_time(activity_name)))

        self.__info_frame.grid(row=0, column=0, sticky='we')
    def set_time(self, activity_name):
        time_spent = self.__time_spent.get()

        blank_message = Label(self.__info_frame, text="", fg= self.theme['warning'], bg= self.theme['overlay'])
        blank_message.grid(row=3, column=0, padx=5, sticky=W)

        numeric_message = Label(self.__info_frame, text="", fg= self.theme['warning'], bg= self.theme['overlay'])
        numeric_message.grid(row=3, column=0, padx=5,  sticky=W)

        success_message = Label(self.__info_frame, text="", fg= self.theme['success'], bg= self.theme['overlay'])
        success_message.grid(row=3, column=0, sticky=W)

        if(time_spent == ""):
            blank_message['text'] = "Fill hours spent"
        elif(not(time_spent.isnumeric())):
            numeric_message['text'] = "Please input a number"
        else:
            ActivityController().set_activity_progress(activity_name, int(time_spent))
            success_message['text'] = "Success!"






# ActivityTimeWindow("act1").display()