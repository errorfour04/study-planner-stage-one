import tkinter as tk

class BaseWindow:
    def __init__(self):
        # Theme
        self.theme = dict(
            # Old Theme Colour #e6e6e6
            bg='#e6e6e6',
            overlay='#ffffff',
            dull='#999999',
            heading='#659df7',
            success='#42f474',
            warning='#f9e354',
            danger='#f9364a',
        )
        self.font = dict(label='Times 20 bold')

        self.root = tk.Tk()
        self.root.title('Window Title')
        self.root.tk_setPalette(self.theme['bg'])

        # Rowcount (rc) will allow for dynamic gridding
        self.rowcount = 0
        self.root.geometry('{}x{}+{}+{}'.format(1000, 600, 0, 0))
        self.root.resizable(False, False)

        # remove menu by default
        self.root.config(menu=tk.Menu())

    def position(self, x, y):
        x_pos = (self.root.winfo_screenwidth() - x) / 2
        y_pos = (self.root.winfo_screenheight() - y) / 3
        self.root.geometry('+{}+{}'.format(int(x_pos), int(y_pos)))

    def display(self):
        self.root.mainloop()

    def hide(self):
        self.root.withdraw()

    def show(self):
        self.root.deiconify()

    def popup(self):
        self.root.grab_set()
        self.root.focus_force()

    # Increment then return rc
    def rc(self):
        self.rowcount += 1
        return self.rowcount

    # Create widgets in the same row but on different columns
    def rcc(self):
        return self.rowcount