from Model.Assignment import Assignment

from datetime import datetime, timedelta
import tkinter as tk

class GanttChart:
    def __init__(self, master, assignment, width, height, start_date, num, primary=None, secondary=None,):
        self.__master = master
        self.__assignment = assignment
        self.__width = width
        self.__height = height
        self.__start_date = start_date
        self.__end_date = None
        self.__num = num
        self.__primary = str(primary) if primary is not None else 'red'
        self.__secondary = str(secondary) if secondary is not None else 'blue'

        # constants
        self.__font = '-size 10'
        self.__left_x = int(width*0.25)
        self.__right_x = int(width - 100)
        self.__top_y = int(height*0.1)
        self.__bottom_y = int(height - 100)

        self.__prong_width = 5
        self.__prong_start = int(self.__left_x - self.__prong_width)
        self.__prong_start_x = int(self.__bottom_y + self.__prong_width)
        self.__num_x_prongs = 10

        self.__y_axis_height = abs(self.__top_y - self.__bottom_y)
        self.__x_axis_width = abs(self.__right_x - self.__left_x)

        self.__y_interval = int(self.__y_axis_height / self.__num)
        self.__x_interval = int(self.__x_axis_width / self.__num_x_prongs)


        # Create Canvas
        self.__canvas = tk.Canvas(master, width=self.__width, height=self.__height, bg='white')

        self.create_title()
        self.create_axes()
        self.create_x_labels()
        self.create_y_labels()
        self.create_keys()

    def create_title(self):
        title = tk.Label(self.__canvas, text=a.get_title(), bg='white', font='-size 20 -weight bold')

        self.__canvas.create_window(
            int(self.__width / 2),
            int(self.__height * 0.05),
            window=title
        )

    def create_axes(self):
        x_axis = self.__canvas.create_line(self.__left_x, self.__top_y, self.__left_x, (self.__bottom_y + 5))
        y_axis = self.__canvas.create_line((self.__left_x - 5), self.__bottom_y, self.__right_x, self.__bottom_y)

        # create prongs on y axis
        for i in range(self.__num):
            self.__canvas.create_line(
                self.__prong_start,
                int(self.__top_y + (i * self.__y_interval)),
                self.__left_x,
                int(self.__top_y + (i * self.__y_interval))
            )

        # create prongs on y axis
        for i in range(self.__num_x_prongs):
            self.__canvas.create_line(
                self.__left_x + ((i + 1) * self.__x_interval),
                self.__prong_start_x,
                self.__left_x + ((i + 1) * self.__x_interval),
                self.__bottom_y
            )

    def create_x_labels(self):
        date = self.__start_date

        for i in range(self.__num_x_prongs + 1):
            fmt_date = date.strftime("%b %d")
            date = date + timedelta(days=7)

            date_label = tk.Label(self.__canvas, text='{}'.format(fmt_date), bg='white', font=self.__font)

            self.__canvas.create_window(
                self.__left_x + (i * self.__x_interval),
                self.__bottom_y + 30,
                window=date_label
            )

        self.__end_date = date - timedelta(days=7)

    def create_y_labels(self):
        x = self.__left_x

        for i in range(self.__num):
            task = self.__assignment.get_tasks()[i]
            y = self.__top_y + (self.__y_interval * i) + (self.__y_interval / 2)

            task_label = tk.Label(
                self.__canvas,
                text='{}'.format(task.get_title()),
                bg='white',
                font=self.__font,
                justify='right',
            )

            self.__canvas.create_window((x - 125), y, window=task_label)

            # Get starting position
            time_diff_1 = task.get_start_date() - self.__start_date
            time_diff_2 = self.__end_date - self.__start_date
            td1 = timedelta(days=time_diff_1.days).days
            td2 = timedelta(days=time_diff_2.days).days

            task_begin = (td1/td2)

            # Get ending position
            time_diff_3 = task.get_end_date() - task.get_start_date()
            td3 = timedelta(days=time_diff_3.days).days

            task_end = (td3 / td2)

            # print(task_begin)
            # Determine bar ending position
            if task.get_start_date() >= self.__start_date and task.get_end_date() <= self.__end_date:
                # Draw bars
                self.__canvas.create_rectangle(
                    x + int(self.__x_axis_width * task_begin),
                    y - 5,
                    x + int(self.__x_axis_width * task_begin) + int(self.__x_axis_width * task_end),
                    y + 5,
                    fill=self.__primary,
                    width=0
                )

    def create_keys(self):
        complete_key_label = tk.Label(self.__canvas, text='Complete', bg='white', font=self.__font)
        incomplete_key_label = tk.Label(self.__canvas, text='Incomplete', bg='white', font=self.__font)

        y = int(self.__height * 0.95)

        self.__canvas.create_window(
            int(self.__width * 0.4),
            y,
            window=complete_key_label
        )

        self.__canvas.create_window(
            int(self.__width * 0.6),
            y,
            window=incomplete_key_label
        )

        # Complete Square
        self.__canvas.create_rectangle(
            int(self.__width * 0.4) + 40,
            y - 5,
            int(self.__width * 0.4) + 50,
            y + 5,
            fill=self.__primary,
            width=0
        )

        # Incomplete Square
        self.__canvas.create_rectangle(
            int(self.__width * 0.6) + 40,
            y - 5,
            int(self.__width * 0.6) + 50,
            y + 5,
            fill=self.__secondary,
            width=0
        )


    def create(self):
        return self.__canvas

a = Assignment('General Studies Essay', 'Coursework', '09/03/19', 30)
root = tk.Tk()

gc = GanttChart(root, a, 1000, 500, datetime.now(), len(a.get_tasks()), 'blue', 'red')
canvas = gc.create()
canvas.pack()

root.mainloop()

