import tkinter as tk

from View.BaseWindow import BaseWindow


class AssignmentWindow(BaseWindow):
    def __init__(self, data):
        super(AssignmentWindow, self).__init__()
        self.data = data

        self.root.title('{} - {}'.format(data['module'], data['title']))
        self.root.minsize(1000, 675)
        self.root.geometry('1000x670')
        self.position(1000, 675)
        self.root.resizable(False, False)
        self.root.grid_columnconfigure(0, weight=1)

        # Back Button
        back_button = tk.Button(self.root, text='Return to Modules Page', command=self.back_button_clicked)
        back_button.grid(row=0, padx=10, pady=(10,5), sticky='w')

        assignment_title = tk.Label(self.root, text=data['title'], font='-size 20 -weight bold')
        type_label = tk.Label(self.root, text='{} worth {}%'.format(data['type'], data['weight']))
        deadline_label = tk.Label(self.root, text='{}'.format(data['deadline']), fg=self.theme['heading'])
        task_frame = tk.Frame(self.root, background=self.theme['overlay'])
        task_frame.grid_columnconfigure(1, weight=1)
        milestone_frame = tk.Frame(self.root, background=self.theme['overlay'])
        milestone_frame.grid_columnconfigure(0, weight=1)
        # milestone_frame.grid_columnconfigure(1, weight=1)
        add_task_button = tk.Button(
            self.root,
            text='Add New Task',
            fg=self.theme['overlay'],
            bg=self.theme['heading'],
            default='active',
            command=self.add_task_button_clicked
        )

        # Inside Milestone Frame
        milestone_heading = tk.Label(
            milestone_frame,
            text='Milestones',
            font='-size 16 -weight bold',
            fg=self.theme['heading'],
            bg=self.theme['overlay']
        )
        add_milestone_button = tk.Label(
            milestone_frame, text='+', font='Calibri 25 bold', fg=self.theme['success'], bg=self.theme['overlay']
        )
        milestone_scroll = tk.Scrollbar(milestone_frame)
        milestone_canvas = tk.Canvas(
            milestone_frame,
            bg=self.theme['overlay'],
            bd=-5,
            height=100,
            yscrollcommand=milestone_scroll.set,
        )
        milestone_scroll.config(command=milestone_canvas.yview)
        milestone_frame.bind(
            '<Configure>', lambda e, canvas=milestone_canvas: canvas.config(scrollregion=canvas.bbox('all'))
        )

        # Inside Milestone Canvas
        self.milestone_frame = tk.Frame(milestone_canvas, bg=self.theme['overlay'])
        self.milestone_frame.grid_columnconfigure(0, weight=1)

        # Position Widgets Inside Milestone Canvas
        milestone_frame_window = milestone_canvas.create_window(5, 5, anchor='nw', window=self.milestone_frame)
        milestone_canvas.itemconfig(milestone_frame_window, width=milestone_canvas.winfo_reqwidth() * 2.53)

        # Position widgets inside Milestone frame
        milestone_heading.grid(row=0, column=0, padx=10, pady=(5, 0), sticky='w')
        add_milestone_button.grid(row=0, column=1, columnspan=2, padx=10, sticky='e')
        add_milestone_button.bind('<Button-1>', lambda e: self.add_milestone_button_clicked())
        milestone_canvas.grid(row=1, columnspan=2, padx=(10, 0), pady=(0, 5), sticky='nsew')
        milestone_scroll.grid(row=1, column=2, padx=(0, 10), pady=(0, 5), sticky='nsew')

        # Inside Task Frame
        task_frame_heading = tk.Label(
            task_frame,
            text='Associated Tasks',
            font='-size 16 -weight bold',
            fg=self.theme['heading'],
            bg=self.theme['overlay']
        )
        task_table_heading1 = tk.Label(task_frame, text='Due date', font='Arial 10 bold', bg=self.theme['overlay'])
        task_table_heading2 = tk.Label(task_frame, text=' Description', font='Arial 10 bold', bg=self.theme['overlay'])
        task_table_heading3 = tk.Label(task_frame, text='Completion', font='Arial 10 bold', bg=self.theme['overlay'])
        task_scroll = tk.Scrollbar(task_frame)
        task_canvas = tk.Canvas(
            task_frame,
            bg=self.theme['overlay'],
            bd=-5,
            yscrollcommand=task_scroll.set,
        )
        task_scroll.config(command=task_canvas.yview)
        self.root.bind('<Configure>', lambda e, canvas=task_canvas: canvas.config(scrollregion=canvas.bbox('all')))

        # Inside Task Canvas
        self.canvas_frame = tk.Frame(task_canvas, bg=self.theme['overlay'])
        self.canvas_frame.grid_columnconfigure(1, weight=1)

        # Position widgets inside Task Canvas
        canvas_frame_window = task_canvas.create_window(5, 5, anchor='nw', window=self.canvas_frame)
        task_canvas.itemconfig(canvas_frame_window, width=task_canvas.winfo_reqwidth() * 2.55)

        # Position widgets inside Task Frame
        task_frame_heading.grid(row=0, columnspan=2, padx=10, pady=5, sticky='w')
        task_table_heading1.grid(row=1, column=0, padx=(10,0), pady=5, sticky='w')
        task_table_heading2.grid(row=1, column=1, pady=5, sticky='w')
        task_table_heading3.grid(row=1, column=2, padx=(0,10), pady=5, sticky='w')
        task_canvas.grid(row=2, columnspan=3, padx=(10,0), pady=(0,5), sticky='nsew')
        task_scroll.grid(row=2, column=5, padx=(0,10), pady=(0,5), sticky='nsew')

        # Position other widgets
        assignment_title.grid(row=1, padx=10, sticky='w')
        type_label.grid(row=2, padx=10, sticky='w')
        deadline_label.grid(row=3, padx=10, sticky='w')
        milestone_frame.grid(row=4, padx=10, pady=10, sticky='nsew')
        task_frame.grid(row=5, padx=10, pady=(10,0), sticky='news')
        add_task_button.grid(row=6, columnspan=4, padx=10, sticky='nsew')

        # Populate Tasks
        self.populate_milestones(data['milestones'], data['milestone_progress'])
        self.populate_tasks(data['tasks'])

    def populate_milestones(self, milestones, progresses):
        counter = 0
        for milestone in milestones:
            title = tk.Label(self.milestone_frame, text=milestone, bg=self.theme['overlay'])

            title.grid(row=counter, column=0, padx=3, sticky='w')
            title.bind('<Button-1>', lambda e, index=counter: self.milestone_row_clicked(index))

            counter += 1

        counter = 0
        for progress in progresses:
            progress = tk.Label(
                self.milestone_frame,
                text='{}%'.format(progress),
                bg=self.theme['overlay']
            )

            progress.grid(row=counter, column=1, sticky='e')
            progress.bind('<Button-1>', lambda e, index=counter: self.milestone_row_clicked(index))

            counter += 1

    def populate_tasks(self, tasks):
        counter = 0
        for task in tasks:
            date = tk.Label(self.canvas_frame, text=task[0], bg=self.theme['overlay'])
            description = tk.Label(self.canvas_frame, text=task[1], bg=self.theme['overlay'])
            completion = tk.Label(self.canvas_frame, text='{}%'.format(task[2]), bg=self.theme['overlay'])

            date.grid(row=counter, column=0, padx=(5, 0), pady=5, sticky='w')
            description.grid(row=counter, column=1, pady=5, sticky='w')
            completion.grid(row=counter, column=2, padx=(0,10), pady=5, sticky='e')

            # Letting the tasks be double clicked on
            date.bind('<Button-1>', lambda e, index=counter: self.task_row_clicked(index))
            description.bind('<Button-1>', lambda e, index=counter: self.task_row_clicked(index))
            completion.bind('<Button-1>', lambda e, index=counter: self.task_row_clicked(index))

            counter += 1

    def add_task_button_clicked(self):
        from Controller.AssignmentController import AssignmentController
        AssignmentController.display_add_task_window()

    def task_row_clicked(self, index):
        from Controller.AssignmentController import AssignmentController
        AssignmentController.load_task(index)

    def add_milestone_button_clicked(self):
        from Controller.AssignmentController import AssignmentController
        AssignmentController.display_create_milestone_window()

    def milestone_row_clicked(selfs, index):
        from Controller.AssignmentController import AssignmentController
        AssignmentController.display_view_milestone_window(index)

    def back_button_clicked(self):
        from Controller.ModuleController import ModuleController
        ModuleController.module_window.show()
        self.root.winfo_children().clear()
        self.root.destroy()



