from tkinter import *
from Controller.MilestoneController import MilestoneController
from Controller.ActivityController import ActivityController
from View.BaseWindow import BaseWindow
from Utilities.Datastore import Datastore
import datetime


class CreateMilestoneWindow(BaseWindow):

    __CreateMilestoneButton = None
    __MilestoneName = None
    __MilestoneWindow = None
    __MilestoneController = None
    __NameTextElement = None
    __DeadlineTextElement = None
    __AssociatedTasks = [] # Checkbutton array
    __AssociatedTasksCheck = None
    __Tasks = None
    __SuccessMessage = None
    __info_frame = None
    __data_frame = None

    # constructor
    def __init__(self, assignment_name):
        #self.__MilestoneController = MilestoneController
        #self.__AssignmentController = AssignmentController
        super(CreateMilestoneWindow, self).__init__()
        print("STARTING", assignment_name)



        # create the window
        self.root.title("Create Milestone for " + assignment_name)
        self.root.geometry('{}x{}'.format(1000, 600))
        self.position(1000, 600)
        self.root.resizable(False, False)
        self.root.tk_setPalette("white")

        self.__MilestoneController = MilestoneController()


        self.__Tasks = []
        self.__AssociatedTasksCheck = []
        self.__AssociatedTasks = []
        #info frame
        self.__info_frame = Frame(self.root, bg=self.theme['overlay'], width=100)
        self.__data_frame = Frame(self.root, bg=self.theme['overlay'], width=500)


        # title
        title_label = Label(self.__info_frame, text="Create Milestone", font='Times 20 bold',
                            bg = self.theme['overlay'])
        title_label.grid(row=0, column = 5, columnspan=2, padx=10, pady=5, sticky=W)

        # NAME
        name_text_label = Label(self.__data_frame, bg=self.theme['overlay'],
                                     text="Name of Milestone: ", font=self.font['label'])
        name_text_label.grid(row=0, column=0, sticky=W)
        self.__NameTextElement = Entry(self.__data_frame,
                                     bg=self.theme['overlay'],
                                     )
            # position NAME
        self.__NameTextElement.grid(row=1, column=0, padx=10, pady=5, sticky=W)


        # DEADLINE

        # initiate Deadline text element
        deadline_text_label = Label(self.__data_frame, bg=self.theme['overlay'], text="Deadline of Milestone: ",
                                    font=self.font['label'])
        deadline_text_label.grid(row=2, column=0, sticky=W)

        self.__DeadlineTextElement = Entry(self.__data_frame,
                                     bg=self.theme['overlay'],
                                     width=20)
        self.__DeadlineTextElement.grid(row=3, column=0,  padx=10, pady=5, sticky=W)





        # TASKS
        tasks = Datastore.get_tasks(assignment_name)
        self.__Tasks = tasks


        # tasks frame
        master_scroll_frame = Frame(self.root, bg=self.theme['overlay'])
        master_scroll_frame.grid(row=5, column=0, sticky=W)

        tasks_scroll = Scrollbar(master_scroll_frame)
        tasks_scroll.grid(row=0, column=1, sticky='nsew')

        scroll_canvas = Canvas(master_scroll_frame, width=500, height=250, bg=self.theme['overlay'],
                               yscrollcommand=tasks_scroll.set)
        scroll_canvas.grid(row=0, column=0, padx=5, sticky='nsew')

        tasks_scroll.config(command=scroll_canvas.yview)
        scroll_canvas.bind('<Configure>',
                           lambda e, canvas=scroll_canvas: canvas.config(scrollregion=canvas.bbox('all')))


        tasks_frame = Frame(scroll_canvas, bg=self.theme['overlay'], width=250)

        tasks_text_label = Label(self.__data_frame, bg=self.theme['overlay'],
                                 text="Tasks that contribute to Milestone:", font=self.font['label'])
        tasks_text_label.grid(row=5, column=0, pady=10, sticky=W)


        for i in tasks:
            #convert datetime to str

            end_date = i.get_end_date().strftime("%Y-%m-%d %H:%M:%S")
            but = Checkbutton(tasks_frame,text=i.get_title(),variable=tasks.index(i),
                              bg=self.theme['overlay'])
            but.grid(row= tasks.index(i), column = 0, padx= 5, sticky=W)
            # deadline label
            deadline_label = Label(tasks_frame, text="Deadline: "+end_date, bg=self.theme['overlay'])
            deadline_label.grid(row=tasks.index(i), column=1, padx=5, sticky=W)
            self.__AssociatedTasksCheck.append(but)


        task_window = scroll_canvas.create_window(0,0,anchor='nw',window=tasks_frame)


        
        self.__CreateMilestoneButton = Button(self.root, text="Submit", \
                                                command=lambda: self.create_milestone_button_clicked(assignment_name),
                                                bg=self.theme['dull'],)
        self.__CreateMilestoneButton.grid(row=6, column=1, padx=5,  sticky=W)

        self.__info_frame.grid(row=1, column=0, padx=10, pady=10, sticky='we')
        self.__data_frame.grid(row=2, column=0, padx=10, pady=10, sticky='we')
      #  tasks_frame.grid(row=5, column=0, sticky='we')

        self.__back_button = Button(self.root, text="Back", bg=self.theme['dull'])
        self.__back_button.grid(row=7, column=0, sticky=W)
        self.__back_button.bind('<Button-1>', (lambda e: self.back()))
        print(self.__AssociatedTasksCheck)

    def get_associated_tasks(self):
        associated_tasks = []
        for i in self.__AssociatedTasks:
            associated_tasks.append(self.__AssociatedTasks['text'])

        return associated_tasks

    def displayMessage(self, success):
        if success is True:
            self.__SuccessMessage['text'] = "Success!"
        else:
            self.__SuccessMessage['text'] = "Fail!"


    def check_before_task(self):
        count = 0

        for i in self.__AssociatedTasksCheck:
            selected_deadline = self.__Tasks[count].get_end_date()
            input_deadline = datetime.datetime.strptime(self.__DeadlineTextElement.get(), "%Y-%m-%d %H:%M")
            if(int(i.getvar(str(count))) == 1 and
                selected_deadline > input_deadline):
                    return False

            count += 1

        return True
    def back(self):
        from Controller.AssignmentController import AssignmentController
        AssignmentController().assignment_window.show()
        self.root.destroy()

    def create_milestone_button_clicked(self, assignment_name):
        name = self.__NameTextElement.get()
        deadline = self.__DeadlineTextElement.get()
        tasks_to_complete = []

        count = 0
        for i in self.__AssociatedTasksCheck:
            if(int(i.getvar(str(count))) == 1):
                tasks_to_complete.append(i['text'])

            count += 1


        # valid test conditions
        name_filled = True
        deadline_filled = True
        deadline_before = True
        tasks_selected = True
        valid_date = True

        # clear messages
        self.__SuccessMessage = Label(self.__data_frame, text=' '*100,
                                      bg=self.theme['overlay'], fg=self.theme['warning'])

        self.__SuccessMessage.grid(row=1, column=1, sticky=W)
        self.__SuccessMessage = Label(self.__data_frame, text=' '*100,
                                      bg=self.theme['overlay'], fg=self.theme['warning'])
        self.__SuccessMessage.grid(row=3, column=1, sticky=W)
        self.__SuccessMessage = Label(self.__data_frame, text=' '*100,
                                      bg=self.theme['overlay'], fg=self.theme['warning'])
        self.__SuccessMessage.grid(row=4, column=1, sticky=W)

        #self.__SuccessMessage.grid(row=i, column=1, sticky=W)
        # validation
        if(name is ""):
            self.__SuccessMessage = Label(self.__data_frame, text="Please fill in Name field",
                                          bg=self.theme['overlay'], fg=self.theme['warning'])
            self.__SuccessMessage.grid(row=1, column=1, sticky=W)
            name_filled = False
        if(deadline is ""):
            self.__SuccessMessage = Label(self.__data_frame, text="Please fill in Deadline field",
                                          bg=self.theme['overlay'], fg=self.theme['warning'])
            self.__SuccessMessage.grid(row=3, column=1, sticky=W)
            deadline_filled = False

        # time validation
        elif(not(len(deadline) == 16)):
            print("len")
            self.__SuccessMessage = Label(self.__data_frame, text="Invalid time format. Please use: YYYY-MM-DD hh:mm",
                                          bg=self.theme['overlay'], fg=self.theme['warning'])
            self.__SuccessMessage.grid(row=3, column=1, sticky=W)
            valid_date = False
        elif(deadline[4] != '-'):
            print("invalid -")
            self.__SuccessMessage = Label(self.__data_frame, text="Invalid time format. Please use: YYYY-MM-DD hh:mm",
                                          bg=self.theme['overlay'], fg=self.theme['warning'])
            self.__SuccessMessage.grid(row=3, column=1, sticky=W)
            valid_date = False
        elif(deadline[7] != '-'):
            print("invalid -")
            self.__SuccessMessage = Label(self.__data_frame, text="Invalid time format. Please use: YYYY-MM-DD hh:mm",
                                          bg=self.theme['overlay'], fg=self.theme['warning'])
            self.__SuccessMessage.grid(row=3, column=1, sticky=W)
            valid_date = False
        elif(deadline[10] != ' '):
            print("invalid   ")
            self.__SuccessMessage = Label(self.__data_frame, text="Invalid time format. Please use: YYYY-MM-DD hh:mm",
                                          bg=self.theme['overlay'], fg=self.theme['warning'])
            self.__SuccessMessage.grid(row=3, column=1, sticky=W)
            valid_date = False
        elif(deadline[13] != ':'):
            print("invalid :")
            self.__SuccessMessage = Label(self.__data_frame, text="Invalid time format. Please use: YYYY-MM-DD hh:mm",
                                          bg=self.theme['overlay'], fg=self.theme['warning'])
            self.__SuccessMessage.grid(row=3, column=1, sticky=W)
            valid_date = False
        elif(not(self.check_before_task())):
            self.__SuccessMessage = Label(self.__data_frame, text="Failed to submit, please ensure"
                                                          " task due dates precede milestone due date",
                                          bg=self.theme['overlay'], fg=self.theme['warning'])
            self.__SuccessMessage.grid(row=7, column=0, sticky=W)
            deadline_before = False

        # tasks complete empty
        if(tasks_to_complete == []):
            self.__SuccessMessage = Label(self.__data_frame, text="Please select at least one task for milestone",
                                          bg=self.theme['overlay'], fg=self.theme['warning'])
            self.__SuccessMessage.grid(row=4, column=1, sticky=W)
            tasks_selected = False


        if(deadline_before is True and name_filled is True and deadline_filled is True and tasks_selected is True and\
            valid_date is True):

            deadline = datetime.datetime.strptime(deadline, "%Y-%m-%d %H:%M")
            self.__MilestoneController.createMilestone(name, deadline, tasks_to_complete, assignment_name)
            self.__SuccessMessage = Label(self.__data_frame, text="Success!", fg = self.theme['success'],
                                          bg = self.theme['overlay'])


            self.__SuccessMessage.grid(row=1, column=1, sticky=W)
            self.root.destroy()





