import tkinter as tk
from tkinter import filedialog, messagebox

from View.BaseWindow import BaseWindow

class CreateStudyProfileWindow(BaseWindow):
    def __init__(self):
        super(CreateStudyProfileWindow, self).__init__()

        self.root.title('Create Semester Study Profile')
        self.root.geometry('{}x{}'.format(400, 200))
        self.position(400,200)
        self.root.resizable(False, False)
        self.root.tk_setPalette(self.theme['overlay'])
        self.root.grid_columnconfigure(0, weight=1)
        self.root.grid_columnconfigure(1, weight=1)

        # Create Widgets
        title_label = tk.Label(self.root, text='Title', fg=self.theme['heading'])
        file_label = tk.Label(self.root, text='Semester File', fg=self.theme['heading'])
        add_semester_file_button = tk.Label(
            self.root, text='+', font='Calibri 25 bold', fg=self.theme['success'], bg=self.theme['overlay']
        )
        add_semester_file_button.bind('<Button-1>', lambda e: self.add_semester_file_button_clicked())

        self.title_entry = tk.Entry(self.root)
        self.file_entry = tk.Entry(self.root)
        cancel_button = tk.Button(self.root, text='Cancel', command=lambda: self.root.destroy())
        create_study_profile_button = tk.Button(
            self.root,
            text='Create Study Profile',
            default='active',
            command=self.create_study_profile_button_clicked,
            fg=self.theme['overlay'],
            bg=self.theme['heading']
        )

        # Position Widgets
        title_label.grid(row=0, column=0, padx=10, sticky='w')
        file_label.grid(row=2, column=0, padx=10, sticky='w')
        add_semester_file_button.grid(row=2, column=1, padx=10, sticky='e')

        self.title_entry.grid(row=1, column=0, columnspan=2, padx=10, sticky='ew')
        self.title_entry.focus_set()
        self.file_entry.grid(row=3, column=0, columnspan=2, padx=10, pady=(0,20), sticky='ew')
        create_study_profile_button.grid(row=4, columnspan=2, padx=10, sticky='ew')
        cancel_button.grid(row=5, columnspan=2, padx=10, sticky='ew')

        self.root.bind('<Return>', lambda e: self.create_study_profile_button_clicked())


    def create_study_profile_button_clicked(self):
        from Controller.MainController import MainController

        profile_title = str(self.title_entry.get()).strip()
        semester_file = str(self.file_entry.get()).strip()

        errors = []
        # Clear Existing Error Displays
        self.title_entry.config(bg=self.theme['overlay'])
        self.file_entry.config(bg=self.theme['overlay'])

        # Check for errors
        if len(profile_title) < 1:
            errors.append('title')

        if len(semester_file) < 1:
            errors.append('semester file')

        # Attempt to pass required info on to the relevant controller
        if len(errors) < 1:
            MainController.create_study_profile(profile_title, semester_file)
        else:
            if 'title' in errors:
                self.title_entry.config(bg='pink')
            if 'semester file' in errors:
                self.file_entry.config(bg='pink')

    def add_semester_file_button_clicked(self):
        # Prompt user to input a file
        semester_file = filedialog.askopenfilename(parent=self.root)

        # Put file path into entry
        self.file_entry.delete(0, tk.END)
        self.file_entry.insert(0, semester_file)

    # Show error message if semester file is invalid
    def display_error(self):
        messagebox.showinfo('Error', "The semester file is invalid. Please provide a valid semester file.")

